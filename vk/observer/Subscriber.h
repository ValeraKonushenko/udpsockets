#pragma once
#include "../namespaces_decl.h"
#include <functional>
#include <atomic>
#include <mutex>


VK_NAMESPACE__VK_SAFE__BEG
class Subscriber {
	public:
		typedef std::atomic<unsigned long long> hash_t;
		typedef std::function<void()> func_t;
		typedef unsigned int event_id;
	private:
		mutable std::recursive_mutex recmut;
		hash_t	hash;
		func_t	func;
		event_id	id;
		static hash_t	hash_counter;
	public:
		void			set					(event_id id,func_t event_func)	noexcept;
		bool			isFunc				()								const noexcept;
						Subscriber			()								noexcept;
						Subscriber			(event_id id,func_t event_func)	noexcept;
		hash_t			getHash				()								const noexcept;
		event_id		getEventId			()								const noexcept;
		void			UDPate				()								const noexcept;
		bool			operator==			(const Subscriber& sb)			const noexcept;
		bool			operator!=			(const Subscriber& sb)			const noexcept;

		virtual			~Subscriber			()								= default;
						Subscriber			(const Subscriber& sb)			= delete;
						Subscriber			(Subscriber&& sb)				noexcept;
						Subscriber& operator=(const Subscriber& sb)			= delete;
						Subscriber& operator=(Subscriber&& sb)				noexcept;

	};
VK_NAMESPACE__VK_SAFE__END



VK_NAMESPACE__VK_UNSAFE__BEG
	class Subscriber {
	public:
		typedef unsigned long long hash_t;
		typedef std::function<void()> func_t;
		typedef unsigned int event_id;
	private:
		hash_t	hash;
		func_t	func;
		event_id	id;
		static hash_t	hash_counter;
	public:
						Subscriber			(event_id id,func_t event_func)	noexcept;
		hash_t			getHash				()								const noexcept;
		event_id		getEventId			()								const noexcept;
		void			UDPate				()								const noexcept;
		bool			operator==			(const Subscriber& sb)			const noexcept;
		bool			operator!=			(const Subscriber& sb)			const noexcept;

		virtual			~Subscriber			()								= default;
						Subscriber			(const Subscriber& sb)			= delete;
						Subscriber			(Subscriber&& sb)				noexcept;
						Subscriber& operator=(const Subscriber& sb)			= delete;
						Subscriber& operator=(Subscriber&& sb)				noexcept;

	};
VK_NAMESPACE__VK_UNSAFE__END