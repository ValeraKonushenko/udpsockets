#pragma once
#include "namespaces_decl.h"
#include <mutex>
VK_NAMESPACE__VK_UDP_SAFE__BEG
class Address {
public:
	using node = unsigned int;
private:
	node n[4];
	int port;
protected:
	mutable std::recursive_mutex recmut;
public:
	std::string getFullAddress()const noexcept(false);
	std::wstring getFullAddressW()const noexcept(false);
	void set(int port, const wchar_t* p)noexcept(false);
	void set(int port, const char* p)noexcept(false);
	node getNode(char i)const noexcept(false);
	int  getPort()const noexcept;
	void set(int port, node a, node b, node c, node d)noexcept(false);
	void set(int port, node a)noexcept(false);
	void setPort(int port)noexcept;
	void clear();
	Address(int port = 9000, node a = 127, node b = 0, node c = 0, node d = 1)noexcept(false);
	bool operator==(const Address& adr)const noexcept;
	bool operator!=(const Address& adr)const noexcept;
	unsigned int getDestAddr()const noexcept;
	Address(const Address&)				noexcept;
	Address(Address&&)					noexcept;
	Address& operator=(const Address&)	noexcept;
	Address& operator=(Address&&)		noexcept;
};
VK_NAMESPACE__VK_UDP_SAFE__END
