#include "SocketServer.h"
#include "SocketConnection.h"
#include <ctime>
#include <fstream>


VK_NAMESPACE__VK_UDP_SAFE__BEG
int UDPSocket::getDataFromClient(Address& adr_answ, size_t size, int wait)noexcept(false) {
	vk::safe::Logger::push("Waiting the response from client");
	Package package_answ(size);
	package_answ.fill(0);
	int answ = -1;
	is_rec_data = true;
	clock_t start_time = clock();
	clock_t wait_for_answ = wait;
	clock_t last_time = 0;
	while (clock() - start_time <= wait_for_answ) {
		if (clock() - last_time >= 1000) {
			int sec = (clock() - start_time) / 1000;
			vk::safe::Logger::push("Waiting: " + std::to_string(sec) + "s/"+std::to_string(wait/1000)+"s");
			last_time = clock();
		}
		int rec = recvfrom(adr_answ, package_answ);
		if (rec != -1) {
			answ = *reinterpret_cast<bool*>(package_answ.getData().data());
			break;
		}
	}
	is_rec_data = false;
	return answ;
}
void UDPSocket::_bind()noexcept(false) {
	std::lock_guard lg(recmut);

	SOCKADDR_IN sin;
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = INADDR_ANY;
	sin.sin_port = htons(static_cast<u_short>(sc.port()));


	if (bind(sc.socket(), (LPSOCKADDR)&sin, sizeof(sin)) == SOCKET_ERROR) 
		throw std::runtime_error("Binding of the socket: " + std::to_string(sc.socket()) + " was failed");

	sc.sockaddr(sin);
	vk::safe::Logger::push("Binding of the socket: "+std::to_string(sc.socket()) + " was successful");
}
void UDPSocket::open(port p)noexcept(false) {
	std::lock_guard lg(recmut);

	if (isOpen())close();

	createSocket(p);
	_bind();

	vk::safe::Logger::push("Server's port: "+std::to_string(p)+" was successful opened");

}
bool UDPSocket::isRecData()const noexcept {
	std::lock_guard lg(recmut);
	return is_rec_data;
}
bool UDPSocket::sendto(Address adr,const char*data,int size)const noexcept(false){
	std::lock_guard lg(recmut);
	sockaddr_in address;
	address.sin_family		= AF_INET;
	address.sin_addr.s_addr = htonl(adr.getDestAddr());
	address.sin_port		= htons(static_cast<short>(adr.getPort()));

	int sent_b = ::sendto(getConnection().socket(),
						  data, size, 0, 
						  reinterpret_cast<sockaddr*>(&address), sizeof(address));
	if(sent_b != size)
		return false;
	return true;
}
bool UDPSocket::sendto(Address adr, const Package& p)const noexcept(false) {
	std::lock_guard lg(recmut);
	
	sockaddr_in address;
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = htonl(adr.getDestAddr());
	address.sin_port = htons(static_cast<short>(adr.getPort()));

 	int sent_b = ::sendto(
		getConnection().socket(),
		p.inspect().data(),
		p.getFullSize(), 
		0,
		reinterpret_cast<sockaddr*>(&address), sizeof(address)
	);

	if (sent_b != p.getFullSize())
		return false;
	return true;
}
bool UDPSocket::sendto(Address adr, Pack& p, int flag) noexcept(false) {
	static vk::safe::IDCounter id_counter;
	using namespace vk::UDP::safe;
	using namespace vk::safe;
	std::lock_guard lg(recmut);
	do {
		if (p.isEmpty())
			throw std::runtime_error("Can't work with empty pack");
		sockaddr_in address;
		address.sin_family = AF_INET;
		address.sin_addr.s_addr = htonl(adr.getDestAddr());
		address.sin_port = htons(static_cast<short>(adr.getPort()));



		//IDS' SETTING
		for (size_t i = 0; i < p.getAmoOfPackages(); i++) {
			Package tmp = p.get(i);
			tmp.setID(id_counter.getNewID());
			tmp.setFlag(flag);
			p.set(i, tmp);
		}



		//SENDING OF A HEADER
		Package		header(p.getBuffSize());
		_PackData	pdata;
		header.setFlag(Package::Flag::header);
		pdata.amo_packages = p.getAmoOfPackages();
		pdata.beg = p.getCRef(0).getId();
		pdata.end = p.getCRef(p.getAmoOfPackages() - 1).getId();

		header.set(sizeof(pdata), &pdata);
		int sent_b = ::sendto(
			getConnection().socket(),
			header.inspect().data(),
			header.getFullSize(),
			0,
			reinterpret_cast<sockaddr*>(&address), sizeof(address)
		);
		if (sent_b != header.getFullSize())
			throw std::runtime_error("Not full pack of data was sent");



		//GETTING OF THE ANSWER FROM CLIENT
		Address adr_answ;
		int answ = getDataFromClient(adr_answ, p.getBuffSize(), 9000);
		if (answ == -1) {
			Logger::push("Ansewer: NO ANSWER from the client: " + adr_answ.getFullAddress());
			return false;
		} else if (answ) {
			Logger::push("Ansewer: BMP header was got: true");
			for (size_t i = 0; i < p.getAmoOfPackages(); i++) {
				sent_b = ::sendto(
					getConnection().socket(),
					p.getCRef(i).inspect().data(),
					p.getCRef(i).getFullSize(),
					0,
					reinterpret_cast<sockaddr*>(&address), sizeof(address)
				);
				int loading = static_cast<int>(1.0 * (i + 1) / (p.getAmoOfPackages()) * 100.);
				if (static size_t last = 0; loading - last >= 10) {
					Logger::push("Was sent " + std::to_string(loading) + "%");
					last = loading;
				}
				auto tmp = p.getCRef(i).getData();
				if (sent_b != header.getFullSize())
					throw std::runtime_error("Not full pack of data was sent");

			}
			int resp = getDataFromClient(adr_answ, p.getBuffSize(), 15000);
			if (resp == not_full_pack_of_package | resp == -1) {
				Logger::push("Data didn't reach. Resending");
				continue;
			}
			else if(resp == data_was_got) {
				break;
			}
		} else {
			Logger::push("Ansewer: header was got: false");
		}

	} while (true);

	return true;
}
int UDPSocket::recvfrom(Address& adr, char* buff, size_t buff_sz)const noexcept(false){
	std::lock_guard lg(recmut);
	memset(buff, 0, buff_sz);
	sockaddr_in from;
	int from_sz = sizeof(from);
	int rec_b = ::recvfrom(getConnection().socket(), buff, buff_sz, 0, 
						   reinterpret_cast<sockaddr*>(&from), &from_sz);

	adr.set(
		ntohs(static_cast<short>(from.sin_port)),
		ntohl(from.sin_addr.s_addr)
	);

	return rec_b;
}
int  UDPSocket::recvfrom(Address& adr, Package&p)const noexcept(false) {
	std::lock_guard lg(recmut);
	if (p.isEmpty())
		throw std::runtime_error("Can't work with empty package");
	
	Package::byte* buff = new Package::byte[p.getFullSize()];
	sockaddr_in from;
	int from_sz = sizeof(from);
	int rec_b = ::recvfrom(getConnection().socket(), buff, p.getFullSize(), 0,
						   reinterpret_cast<sockaddr*>(&from), &from_sz);

	if (rec_b != SOCKET_ERROR) {
		Package::byte* fake = buff;

		p.setID(*reinterpret_cast<vk::safe::IDCounter::id_t*>(fake));
		fake += sizeof(vk::safe::IDCounter::id_t);
		p.setFlag(*reinterpret_cast<Package::byte*>(fake));
		fake += sizeof(Package::byte);
		p.set(p.getBufferSize(), fake);

		adr.set(ntohs(static_cast<short>(from.sin_port)),ntohl(from.sin_addr.s_addr));
	}
	delete[]buff;
	return rec_b;
}
UDPSocket::UDPSocket(const UDPSocket& s) {
	std::lock_guard lg(this->recmut);
	*this = s;
}
UDPSocket& UDPSocket::operator=(const UDPSocket& s) {
	std::scoped_lock lg(recmut, s.recmut);
	this->sc = s.sc;
	this->wsa = s.wsa;
	return *this;
}
UDPSocket::UDPSocket(UDPSocket&& s) noexcept {
	std::lock_guard lg(recmut);
	*this = std::move(s);
}
UDPSocket& UDPSocket::operator=(UDPSocket&& s) noexcept {
	std::scoped_lock lg(recmut, s.recmut);
	this->sc = s.sc;
	this->wsa = s.wsa;
	return *this;
}
UDPSocket::UDPSocket() {
	is_rec_data = false;
}
VK_NAMESPACE__VK_UDP_SAFE__END











namespace vk::tcp::safe {
bool SocketServer::_bind()noexcept {
	std::lock_guard lg(recmut);

	SOCKADDR_IN sin;
	sin.sin_family			= AF_INET;
	sin.sin_addr.s_addr		= htonl(INADDR_ANY);
	sin.sin_port			= htons(static_cast<u_short>(sc.port()));


	if (bind(sc.socket(), (LPSOCKADDR)&sin, sizeof(sin)) == SOCKET_ERROR) {
		vk::safe::Logger::push(std::move(Log("Binding of the socket: " + std::to_string(sc.socket()) + " was failed" )));
		return false;
	}
	sc.sockaddr(sin);
	printStep((
		"Binding of the socket: " + 
		std::to_string(sc.socket()) + 
		" was successful").c_str());
	return true;
}
bool SocketServer::_listen() noexcept{
	std::lock_guard lg(recmut);

	if (listen(sc.socket(), SOMAXCONN) == SOCKET_ERROR) {
		vk::safe::Logger::push(std::move(Log("Listening of the socket: " + std::to_string(sc.socket()) + " was failed")));
		return false;
	}

	printStep((
		"Listening of the socket: " + 
		std::to_string(sc.socket()) + 
		" was successful").c_str());

	return true;
}
bool SocketServer::open(port p)noexcept {
	std::lock_guard lg(recmut);
	if (isOpen())close();
	if (!this->createSocket(p))	return false;
	if (!this->_bind())			return false;
	if (!this->_listen())		return false;



	printStep((
		"Server's port: " + 
		std::to_string(p) + 
		" was successful opened").c_str());
	return true;
}
SocketServer::SocketServer(const SocketServer& s) {
	std::lock_guard lg(this->recmut);
	*this = s;
}
SocketServer& SocketServer::operator=(const SocketServer& s) {
	std::scoped_lock lg(recmut, s.recmut);
	this->sc = s.sc;
	this->wsa = s.wsa;
	return *this;
}
SocketServer::SocketServer(SocketServer&& s) noexcept {
	std::lock_guard lg(recmut);
	*this = std::move(s);
}
SocketServer& SocketServer::operator=(SocketServer&& s) noexcept {
	std::scoped_lock lg(recmut, s.recmut);
	this->sc = s.sc;
	this->wsa = s.wsa;
	return *this;
}

}




namespace vk::tcp::unsafe {
bool SocketServer::_bind()noexcept {
	SOCKADDR_IN sin;
	sin.sin_family			= AF_INET;
	sin.sin_addr.s_addr		= htonl(INADDR_ANY);
	sin.sin_port			= htons(static_cast<u_short>(sc.port()));


	if (bind(sc.socket(), (LPSOCKADDR)&sin, sizeof(sin)) == SOCKET_ERROR) {
		vk::safe::Logger::push(std::move(Log("Binding of the socket: " + std::to_string(sc.socket()) + " was failed")));
		return false;
	}
	sc.sockaddr(sin);

	printStep((
		"Binding of the socket: " +
		std::to_string(sc.socket()) +
		" was successful").c_str());

	return true;
}

bool SocketServer::_listen() noexcept{
	if (listen(sc.socket(), SOMAXCONN) == SOCKET_ERROR) {
		vk::safe::Logger::push(std::move(Log("Listening of the socket: " + std::to_string(sc.socket()) + " was failed")));
		return false;
	}


	printStep((
		"Listening of the socket: " +
		std::to_string(sc.socket()) +
		" was successful").c_str());
	return true;
}
bool SocketServer::open(port p)noexcept {
	if (isOpen())close();
	if (!this->createSocket(p))	return false;
	if (!this->_bind())			return false;
	if (!this->_listen())		return false;
	printStep((
		"Server's port: " +
		std::to_string(p) +
		" was successful opened").c_str());
	return true;
}
SocketServer::SocketServer(const SocketServer& s) {
	*this = s;
}
SocketServer& SocketServer::operator=(const SocketServer& s) {
	this->sc = s.sc;
	this->wsa = s.wsa;
	return *this;
}
SocketServer::SocketServer(SocketServer&& s) noexcept {
	*this = std::move(s);
}
SocketServer& SocketServer::operator=(SocketServer&& s) noexcept {
	this->sc = s.sc;
	this->wsa = s.wsa;
	return *this;
}

}