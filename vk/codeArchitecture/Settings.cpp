#include "Settings.h"
#include "../Assert.h"
#include <fstream>
VK_NAMESPACE__VK_SAFE__BEG
SettingsData::SettingsData(const char* str):std::string(str){}
SettingsData::SettingsData(){}
int SettingsData::toInt() const  noexcept(false) {
	if (!this->c_str())
		throw vk::exception("String is nullptr", __FILE__, __FUNCTION__, __LINE__);
	return atoi(this->c_str());
}
double SettingsData::toDouble() const noexcept(false) {
	if (!this->c_str())
		throw vk::exception("String is nullptr", __FILE__, __FUNCTION__, __LINE__);
	return atof(this->c_str());
}
const char* SettingsData::toCCharp()	const noexcept(false){
	if (!this->c_str())
		throw vk::exception("String is nullptr", __FILE__, __FUNCTION__, __LINE__);
	return this->c_str();
}
long long SettingsData::toLongLong() const noexcept(false) {
	if (!this->c_str())
		throw vk::exception("String is nullptr", __FILE__, __FUNCTION__, __LINE__);
	return atoll(this->c_str());
}
SettingsData::operator int() const noexcept(false){
	return this->toInt();
}
SettingsData::operator double() const noexcept(false) {
	return this->toDouble();
}
SettingsData::operator long long() const noexcept(false){
	return this->toLongLong();
}





bool Settings::isEmpty() const noexcept{
	return !parser.isEmpty();
}

bool Settings::isProperty(string_view pr) const noexcept{
	std::lock_guard lg(rec_mut);
	return parser.isKey(pr.data());
}
Settings::string Settings::getProperty(string_view pr) const noexcept(false) {
	std::lock_guard lg(rec_mut);
	if(parser.isEmpty())
		throw vk::exception("No data. Use function for loading data to this object", __FILE__, __FUNCTION__, __LINE__);
	auto res = parser[pr.data()];
	if (res.isEmpty())throw vk::exception("No such property: " + std::string(pr), __FILE__, __FUNCTION__, __LINE__);
	return string(res);
}
Settings::string Settings::getProperty(size_t i) const noexcept(false){
	std::lock_guard lg(rec_mut);
	if (parser.isEmpty())
		throw vk::exception("No data. Use function for loading data to this object", __FILE__, __FUNCTION__, __LINE__);
	auto res = parser[i];
	if (res.isEmpty())throw vk::exception("No such property", __FILE__, __FUNCTION__, __LINE__);
	return string(res);
}
Settings::string Settings::operator[](string_view pr) const noexcept(false){
	return getProperty(pr);
}
Settings::string Settings::operator[](size_t i) const noexcept(false){
	return getProperty(i);
}

size_t Settings::countOfProperty() const noexcept(false){
	std::lock_guard lg(rec_mut);
	return parser.size();
}
Settings& Settings::setProperty(string_view property, string value) noexcept(false){
	std::lock_guard lg(rec_mut);
	if (parser.isEmpty())
		throw vk::exception("No data. Use function for loading data to this object", __FILE__, __FUNCTION__, __LINE__);
	auto res = parser[property.data()];
	if (!res)throw vk::exception("No such property: " + std::string(property), __FILE__, __FUNCTION__, __LINE__);
	res->set(value.c_str(), value.length() + 1);
	return *this;
}

Settings& Settings::loadSettings() noexcept(false){
	std::lock_guard lg(rec_mut);
	if (path_to_settings == "")
		throw vk::exception("Path to a file is empty", __FILE__, __FUNCTION__, __LINE__);
	if (parser.setDataFromFile(path_to_settings.c_str()) !=	ParserStatus::ok) 
		throw vk::exception(string() + "Parse error: " + parser.getInfoAboutStatus() + "File name: " + path_to_settings, __FILE__, __FUNCTION__, __LINE__);
	return *this;
}
Settings& Settings::saveSettings() noexcept(false){
	std::lock_guard lg(rec_mut);
	using namespace std;
	fstream file;
	auto data = parser.inspect("\r\n");
	file.open(path_to_settings.c_str(), ios::out | ios::binary);
	if (!file.is_open())
		throw vk::exception("File can't be saved. Has occured some error. May be, you have no enough of rights", __FILE__, __FUNCTION__, __LINE__);
	file.write(data.data(), data.getSize() - 1);
	file.close();
	return *this;
}

Settings::string Settings::getPath() const noexcept{
	std::lock_guard lg(rec_mut);
	return path_to_settings;
}
Settings& Settings::setPath(const char*path) noexcept(false){
	std::lock_guard lg(rec_mut);
	if (!path)
		throw vk::exception("Path to a file is nullptr", __FILE__, __FUNCTION__, __LINE__);
	clear();
	path_to_settings = path;
	return *this;
}
Settings& Settings::clear() noexcept{
	std::lock_guard lg(rec_mut);
	parser.clear();
	path_to_settings.clear();
	return *this;
}
Settings::Settings()noexcept {
}
Settings::~Settings(){

}
VK_NAMESPACE__VK_SAFE__END

