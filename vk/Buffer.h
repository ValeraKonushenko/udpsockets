#pragma once
#include "namespaces_decl.h"
#include "Container.h"
#include <memory>
#include <mutex>

VK_NAMESPACE__VK_SAFE__BEG
class Buffer {
public:
	typedef char bufftype;
	typedef vk::unsafe::Container<bufftype> Cont;
private:
	bufftype*				arr;
	size_t					size;
	void			__init_clear	()									noexcept;
protected:
	mutable std::recursive_mutex	recmut;
public:
	void			clear			()									noexcept;
	void			alloc			(size_t len);
	void			realloc			(size_t len);
	void			set				(bufftype ch)						noexcept;
	void			set				(const bufftype *p, size_t psize)	noexcept;
	void			set				(Buffer& buff)						noexcept;
	bool			isEmpty			()									const noexcept;
	size_t			lenght			()									const;
	size_t			getSize			()									const noexcept;
	Buffer&			operator=		(std::string str)					noexcept;
	Buffer&			operator+=		(std::string str)					noexcept;
	Buffer&			operator=		(const bufftype* str)				noexcept;
	Buffer&			operator+=		(const bufftype* str)				noexcept;
	Cont			get				()									const noexcept;
	bufftype		operator[]		(size_t pos)						const noexcept;
	bufftype		setAt			(size_t pos, bufftype ch)			noexcept;
	void			swap			(Buffer& buff)						noexcept;
	Buffer							(const bufftype* p, size_t psize)	noexcept;
	Buffer							(size_t start_len = 0)				noexcept;
	Buffer							(const bufftype*str)				noexcept;
	Buffer							(const Buffer& sc)					noexcept;
	virtual ~Buffer					()									noexcept;
	Buffer							(Buffer&& sc)						noexcept;
	Buffer&			operator=		(const Buffer& sc)					noexcept;
	Buffer&			operator=		(Buffer&& sc)						noexcept;
					operator size_t ()									const noexcept;


};
VK_NAMESPACE__VK_SAFE__END



VK_NAMESPACE__VK_UNSAFE__BEG
class Buffer {
public:
	typedef char bufftype;
private:
	bufftype*				arr;
	size_t					size;
	void			__init_clear	()									noexcept;
protected:
public:
	void			clear			()									noexcept;
	void			alloc			(size_t len);
	void			realloc			(size_t len);
	void			set				(bufftype ch)						noexcept;
	void			set				(const bufftype *p, size_t psize)	noexcept;
	void			set				(Buffer& buff)						noexcept;
	bool			isEmpty			()									const noexcept;
	size_t			lenght			()									const;
	size_t			getSize			()									const noexcept;
	Buffer&			operator=		(std::string str)					noexcept;
	Buffer&			operator+=		(std::string str)					noexcept;
	Buffer&			operator=		(const bufftype* str)				noexcept;
	Buffer&			operator+=		(const bufftype* str)				noexcept;
	std::string		get				()									const noexcept;
	bufftype		operator[]		(size_t pos)						const noexcept;
	bufftype		setAt			(size_t pos, bufftype ch)			noexcept;
	void			swap			(Buffer& buff)						noexcept;
	Buffer							(const bufftype* p, size_t psize)	noexcept;
	Buffer							(size_t start_len = 0)				noexcept;
	Buffer							(const bufftype*str)				noexcept;
	Buffer							(const Buffer& sc)					noexcept;
	virtual ~Buffer					()									noexcept;
	Buffer							(Buffer&& sc)						noexcept;
	Buffer&			operator=		(const Buffer& sc)					noexcept;
	Buffer&			operator=		(Buffer&& sc)						noexcept;
					operator size_t ()									const noexcept;

};
VK_NAMESPACE__VK_UNSAFE__END