#include "ObjectParser.h"
#include "../Utility.h"
#include "../Assert.h"
#include <utility>
#include <fstream>
namespace vk::unsafe {
ObjectParser::Status ObjectParser::setDataFromFile(const char_t* path) noexcept{
	using namespace std;
	char_t* data = nullptr;
	size_t fsize = 0u;
	fstream file(path, ios::in | ios::binary);
	if (!file.is_open())return last_status = Status::no_such_file;
	file.seekg(0,ios::end);
	fsize = file.tellg();
	file.seekg(0,ios::beg);
	

	data = reinterpret_cast<char_t*>(new char[fsize + sizeof(char_t)]);
	data[fsize / sizeof(char_t)] = 0;
	file.read(reinterpret_cast<char*>(data), fsize);

	file.close();
	last_status = setData(data);
	delete[]data;
	return last_status;
}
ObjectParser::Status ObjectParser::setData(const char_t* data) noexcept{
	clear();
	return parser(data);
}
ObjectParser::Status ObjectParser::push(const char_t* key, const char_t* value) noexcept{
	VK_CHECK_FOR_1B_CHAR(char_t);
	size_t key_len = strlen(key);
	size_t value_len = strlen(value);
	size_t gen_len = key_len + value_len + 1 + 2;//2 - is '=' and ';'
	char_t* buff = new char_t[gen_len]{};
	strcat_s(buff, gen_len, key);
	strcat_s(buff, gen_len, "=");
	strcat_s(buff, gen_len, value);
	strcat_s(buff, gen_len, ";");
	push(buff);
	delete[]buff;
	return last_status = Status::ok;
}
bool ObjectParser::isKey(const char_t* key) const noexcept{
	for (size_t i = 0; i < data.size(); i++) {
		if (strcmp(data[i].key, key) == 0) {
			return true;
		}
	}
	return false;
}
void ObjectParser::pushToContainer(pair& p){
	data.push_back(std::move(p));
}
bool ObjectParser::isValidChar(char_t ch){
	VK_CHECK_FOR_1B_CHAR(char_t);
	if (ch == '\r' || ch == '\n' || ch == ' ' || ch == '\t' || ch == '=' || ch == ';')return false;
	return true;
}
int ObjectParser::getLenOfWord(const char_t* p){
	VK_CHECK_FOR_1B_CHAR(char_t);
	if (!p)return -1;
	int len = 0;
	while (isValidChar(p[len]) && p[len] != ' ')
		len++;
	return len;
}
const ObjectParser::char_t* ObjectParser::goToNextToken(const char_t* p){
	if (!p)return nullptr;
	VK_CHECK_FOR_1B_CHAR(char_t);
	while (*p == ';' || *p == ' ' || *p == 0 || *p == '\r' || *p == '\n' || *p == '\t') {
		if (*p == 0)return nullptr;
		p++;
	}
	return p;
}
const ObjectParser::char_t* ObjectParser::getPointerToKey(const char_t* p){
	if (!p)return nullptr;
	VK_CHECK_FOR_1B_CHAR(char_t);
	while (*p == ' ' || *p == 0 || *p == '\r' || *p == '\n' || *p == '\t') {
		if (*p == 0)return nullptr;
		p++;
	}
	return p;
}
const ObjectParser::char_t* ObjectParser::getPointerToValue(const char_t* p){
	if (!p)return nullptr;
	VK_CHECK_FOR_1B_CHAR(char_t);
	while (*p == ' ' || *p == '\r' || *p == '\n' || *p == '\t')p++;
	return p;
}
const ObjectParser::char_t* ObjectParser::parseKey(const char_t* pkey, key_t& cnt){
	int key_len = getLenOfWord(pkey);
	if (!key_len)return nullptr;
	cnt.reserve(key_len + 1);
	cnt[key_len] = 0;
	for (int i = 0; i < key_len; i++) {
		cnt[i] = *pkey;
		pkey++;
	}

	return pkey;
}
const ObjectParser::char_t* ObjectParser::parseValue(const char_t* pkey, value_t& cnt){
	VK_CHECK_FOR_1B_CHAR(char_t);
	//getting the len before ';'
	int len = 0;
	bool is_str = false;
	int amo_skip = 0;
	if (*pkey == '`') {
		pkey++;
		while (true) {
			if (pkey[len] == '`' && pkey[len-1] != '/')break;
			len++;
			if (pkey[len] == '`' && pkey[len - 1] == '/')amo_skip++;;
		}
		is_str = true;
	} else {
		while ((isValidChar(pkey[len]) || pkey[len] == ' ') && pkey[len] != '\0')
			len++;
	}
	len -= amo_skip;

	//filling to buffer
	cnt.reserve(len + 1);
	cnt[len] = 0;
	for (int i = 0; i < len; i++) {
		if (pkey[0] == '/' && pkey[1] == '`')
			pkey++;
		cnt[i] = *pkey;
		pkey++;
	}
	return pkey + (is_str?1:0);
}
ObjectParser::Status ObjectParser::parser(const char_t* data) noexcept{
	if (!data)return last_status = Status::data_is_nullptr;
	VK_CHECK_FOR_1B_CHAR(char_t);
	pair tmp_pair;
	const char_t* p = data;
	do {
		p = getPointerToKey(p);
		if (!p)return last_status = Status::key_is_nullptr;
		p = parseKey(p, tmp_pair.key);
		if (!p)return last_status = Status::key_parse_error;
		p = strchr(p, '=');
		if (!p)return last_status = Status::token_is_uncorrect;
		p++;
		p = getPointerToValue(p);
		if (!p)return last_status = Status::value_is_nullptr;
		p = parseValue(p, tmp_pair.value);
		if (!p)return last_status = Status::value_parse_error;
		pushToContainer(tmp_pair);
	} while (p = goToNextToken(p));
	return last_status = Status::ok;
}
void ObjectParser::__init__() noexcept{
	data.clear();
	__ObjectParserBase::__init__();
}
bool ObjectParser::isEmpty() const noexcept{
	return data.empty();
}
ObjectParser::value_t* ObjectParser::operator[](const char_t* key) noexcept{
	return this->getValue(key);
}
ObjectParser::value_t* ObjectParser::getValue(const char_t* key) noexcept{
	VK_CHECK_FOR_1B_CHAR(char_t);
	for (size_t i = 0; i < data.size(); i++) {
		if (strcmp(data[i].key , key) == 0) {
			return &data[i].value;
		}
	}	
	return nullptr;
}
ObjectParser::pair* ObjectParser::getPair(const char_t* key) noexcept{
	VK_CHECK_FOR_1B_CHAR(char_t);
	for (size_t i = 0; i < data.size(); i++) {
		if (strcmp(data[i].key, key) == 0) {
			return &data[i];
		}
	}
	return nullptr;
}
ObjectParser::value_t* ObjectParser::operator[](size_t i) noexcept{
	return getValue(i);
}
ObjectParser::value_t* ObjectParser::getValue(size_t i) noexcept{
	if (i >= data.size())return nullptr;
	return &data[i].value;
}
ObjectParser::pair* ObjectParser::getPair(size_t i) noexcept{
	if (i >= data.size())return nullptr;
	return &data[i];
}
ObjectParser::value_t ObjectParser::operator[](const char_t* key) const noexcept{
	return getValue(key);
}
ObjectParser::value_t ObjectParser::getValue(const char_t* key) const noexcept{
	value_t val;
	VK_CHECK_FOR_1B_CHAR(char_t);
	for (size_t i = 0; i < data.size(); i++) {
		if (strcmp(data[i].key, key) == 0) {
			val = data[i].value;
			break;
		}
	}
	return val;
}
ObjectParser::value_t ObjectParser::operator[](size_t i) const noexcept{
	value_t val;
	if (i >= data.size())return val;
	return val = data[i].value;
}
ObjectParser::value_t ObjectParser::getValue(size_t i) const noexcept{
	value_t val;
	return val;
}
void ObjectParser::clear() noexcept{
	data.clear();
	__ObjectParserBase::clear();
}
size_t ObjectParser::size() const noexcept{
	return data.size();
}
ObjectParser::Status ObjectParser::push(const char_t* expr) noexcept{
	return parser(expr);
}
ObjectParser& ObjectParser::operator<<(const char_t* expr) noexcept{
	push(expr);
	return *this;
}
Container<ObjectParser::char_t> ObjectParser::inspect(const char_t* el) const noexcept(false){
	VK_CHECK_FOR_1B_CHAR(char_t)
	if (!el)
		throw std::exception("Was transferred a nullptr");
	Container<char_t> cont;
	size_t sum_len = 0u;
	for (size_t i = 0; i < data.size(); i++) {
		sum_len += data[i].key.getSize() - 1;
		sum_len += data[i].value.getSize() - 1;
		sum_len += 2;
		sum_len += strlen(el);
	}
	cont.reserve(sum_len + 2);
	cont.set(0);
	for (size_t i = 0; i < data.size(); i++) {
		strcat_s(cont.data(), sum_len + 2, data[i].key.toString().c_str());
		strcat_s(cont.data(), sum_len + 2, "=");
		strcat_s(cont.data(), sum_len + 2, data[i].value.toString().c_str());
		strcat_s(cont.data(), sum_len + 2, ";");
		strcat_s(cont.data(), sum_len + 2, el);
	}
	return std::move(cont);
}
ObjectParser::ObjectParser(const char_t* data) noexcept {
	__init__();
	if (data)setData(data);
}
ObjectParser::~ObjectParser(){
	clear();
}
ObjectParser::ObjectParser(const ObjectParser& obj) noexcept{
	*this = obj;
}
ObjectParser::ObjectParser(ObjectParser&& obj) noexcept{
	*this = std::move(obj);
}
ObjectParser& ObjectParser::operator=(const ObjectParser& obj) noexcept{
	data = obj.data;	
	return *this;
}
ObjectParser& ObjectParser::operator=(ObjectParser&& obj) noexcept{
	data = std::move(obj.data);
	obj.__init__();
	return *this;
}









ObjectParserW::Status ObjectParserW::setDataFromFile(const char_t* path) noexcept {
	using namespace std;
	char_t* data = nullptr;
	size_t fsize = 0u;
	fstream file(path, ios::in);
	if (!file.is_open())return last_status = Status::no_such_file;
	file.seekg(ios::end, 0);
	fsize = file.tellg();
	file.seekg(ios::beg, 0);

	data = reinterpret_cast<char_t*>(new char[fsize + sizeof(char_t)]);
	data[fsize / sizeof(char_t)] = 0;
	file.read(reinterpret_cast<char*>(data), fsize);

	file.close();
	auto res = setData(data);
	delete[]data;
	return last_status = res;
}
ObjectParserW::Status ObjectParserW::setData(const char_t* data) noexcept{
	clear();
	return parser(data);	
}
bool ObjectParserW::isKey(const char_t* key) const noexcept{
	for (size_t i = 0; i < data.size(); i++) {
		if (wcscmp(data[i].key, key) == 0) {
			return true;
		}
	}
	return false;
}
void ObjectParserW::pushToContainer(pair& p){
	data.push_back(std::move(p));
}
bool ObjectParserW::isValidChar(char_t ch){
	if (ch == '=' || ch == ';')return false;
	return true;
}
int ObjectParserW::getLenOfWord(const char_t* p){
	if (!p)return -1;
	int len = 0;
	while (isValidChar(p[len]) && p[len] != ' ')
		len++;
	return len;
}
const ObjectParserW::char_t* ObjectParserW::goToNextToken(const char_t* p){
	if (!p)return nullptr;
	while (*p == ';' || *p == ' ' || *p == 0) {
		if (*p == 0)return nullptr;
		p++;
	}
	return p;
}
const ObjectParserW::char_t* ObjectParserW::getPointerToKey(const char_t* p){
	if (!p)return nullptr;
	while (*p == ' ' || *p == 0) {
		if (*p == 0)return nullptr;
		p++;
	}
	return p;
}
const ObjectParserW::char_t* ObjectParserW::getPointerToValue(const char_t* p){
	if (!p)return nullptr;
	while (*p == ' ')p++;
	return p;
}
const ObjectParserW::char_t* ObjectParserW::parseKey(const char_t* pkey, key_t& cnt){
	int key_len = getLenOfWord(pkey);
	if (!key_len)return nullptr;
	cnt.reserve(key_len + 1);
	cnt[key_len] = 0;
	for (int i = 0; i < key_len; i++) {
		cnt[i] = *pkey;
		pkey++;
	}

	return pkey;
}
const ObjectParserW::char_t* ObjectParserW::parseValue(const char_t* pkey, value_t& cnt){
	//getting the len before ';'
	int len = 0;
	bool is_str = false;
	if (*pkey == '`') {
		pkey++;
		while (pkey[len] != '`')len++;
		is_str = true;
	} else {
		while ((isValidChar(pkey[len]) || pkey[len] == ' ') && pkey[len] != '\0')
			len++;
	}

	//filling to buffer
	cnt.reserve(len + 1);
	cnt[len] = 0;
	for (int i = 0; i < len; i++) {
		cnt[i] = *pkey;
		pkey++;
	}
	return pkey + (is_str?1:0);
}
ObjectParserW::Status ObjectParserW::parser(const char_t* data) noexcept{
	if (!data)return last_status = Status::data_is_nullptr;
	pair tmp_pair;
	const char_t* p = data;
	do {
		p = getPointerToKey(p);
		if (!p)return last_status = Status::key_is_nullptr;
		p = parseKey(p, tmp_pair.key);
		if (!p)return last_status = Status::key_parse_error;
		p = wcschr(p, '=');
		if (!p)return last_status = Status::token_is_uncorrect;
		p++;
		p = getPointerToValue(p);
		if (!p)return last_status = Status::value_is_nullptr;
		p = parseValue(p, tmp_pair.value);
		if (!p)return last_status = Status::value_parse_error;
		pushToContainer(tmp_pair);
	} while (p = goToNextToken(p));
	return last_status = Status::ok;	
}
void ObjectParserW::__init__() noexcept{
	data.clear();
	__ObjectParserBase::__init__();
}
bool ObjectParserW::isEmpty() const noexcept{
	return data.empty();
}
ObjectParserW::value_t* ObjectParserW::operator[](const char_t* key) noexcept{
	return this->getValue(key);
}
ObjectParserW::value_t* ObjectParserW::getValue(const char_t* key) noexcept{
	for (size_t i = 0; i < data.size(); i++) {
		if (wcscmp(data[i].key , key) == 0) {
			return &data[i].value;
		}
	}	
	return nullptr;
}
ObjectParserW::pair* ObjectParserW::getPair(const char_t* key) noexcept{
	for (size_t i = 0; i < data.size(); i++) {
		if (wcscmp(data[i].key, key) == 0) {
			return &data[i];
		}
	}
	return nullptr;
}
ObjectParserW::value_t* ObjectParserW::operator[](size_t i) noexcept{
	return getValue(i);
}
ObjectParserW::value_t* ObjectParserW::getValue(size_t i) noexcept{
	if (i >= data.size())return nullptr;
	return &data[i].value;
}
ObjectParserW::pair* ObjectParserW::getPair(size_t i) noexcept{
	if (i >= data.size())return nullptr;
	return &data[i];
}
ObjectParserW::value_t ObjectParserW::operator[](const char_t* key) const noexcept {
	return getValue(key);
}
ObjectParserW::value_t ObjectParserW::getValue(const char_t* key) const noexcept {
	value_t val;
	for (size_t i = 0; i < data.size(); i++) {
		if (wcscmp(data[i].key, key) == 0) {
			val = data[i].value;
		}
	}
	return val;
}
ObjectParserW::value_t ObjectParserW::operator[](size_t i) const noexcept {
	value_t val;
	if (i >= data.size())return val;
	return val = data[i].value;
}
ObjectParserW::value_t ObjectParserW::getValue(size_t i) const noexcept {
	value_t val;
	return val;
}
void ObjectParserW::clear() noexcept{
	data.clear();
	__ObjectParserBase::clear();
}
size_t ObjectParserW::size() const noexcept{
	return data.size();
}
ObjectParserW::Status ObjectParserW::push(const char_t* expr) noexcept{
	return parser(expr);
}
ObjectParserW& ObjectParserW::operator<<(const char_t* expr) noexcept{
	push(expr);
	return *this;
}
Container<ObjectParserW::char_t> ObjectParserW::inspect(const char_t* el) const noexcept(false) {
	if (!el)
		throw std::exception("Was transferred a nullptr");
	Container<char_t> cont;
	size_t sum_len = 0u;
	for (size_t i = 0; i < data.size(); i++) {
		sum_len += data[i].key.getSize() - 1;
		sum_len += data[i].value.getSize() - 1;
		sum_len += 2;
		sum_len += wcslen(el);
	}
	cont.reserve(sum_len + 1);
	cont[0] = cont[sum_len] = 0;
	for (size_t i = 0; i < data.size(); i++) {
		wcscat_s(cont.data(), sum_len + 1, data[i].key);
		wcscat_s(cont.data(), sum_len + 1, L"=");
		wcscat_s(cont.data(), sum_len + 1, data[i].value);
		wcscat_s(cont.data(), sum_len + 1, L";");
		wcscat_s(cont.data(), sum_len + 1, el);
	}
	return std::move(cont);
}
ObjectParserW::ObjectParserW(const char_t* data) noexcept {
	__init__();
	if (data)setData(data);
}
ObjectParserW::~ObjectParserW(){
	clear();
}
ObjectParserW::ObjectParserW(const ObjectParserW& obj) noexcept{
	*this = obj;
}
ObjectParserW::ObjectParserW(ObjectParserW&& obj) noexcept{
	*this = std::move(obj);
}
ObjectParserW& ObjectParserW::operator=(const ObjectParserW& obj) noexcept{
	data = obj.data;	
	return *this;
}
ObjectParserW& ObjectParserW::operator=(ObjectParserW&& obj) noexcept{
	data = std::move(obj.data);
	obj.__init__();
	return *this;
}




void __ObjectParserBase::__init__() noexcept{
	clear();
}
const wchar_t* __ObjectParserBase::getInfoAboutStatusW(Status st)noexcept(false) {
	using vk::unsafe::ObjectParserW;
	switch (st) {
		case Status::none:
		return L"The status has not been set yet";
		case Status::ok:
		return L"OK. No errors";
		case Status::no_such_file:
		return L"A file not found or may be no such file";
		case Status::data_is_nullptr:
		return L"Some transfered data are nullptr";
		case Status::key_is_nullptr:
		return L"Key not found was found nullptr. May be some error with syntax";
		case Status::value_is_nullptr:
		return L"Value not found was found nullptr. May be some error with syntax";
		case Status::key_parse_error:
		return L"Key-parsing error. The source code has the mistake[s] in the syntax";
		case Status::value_parse_error:
		return L"Value-parsing error. The source code has the mistake[s] in the syntax";
		case Status::token_is_uncorrect:
		return L"Some-token-parsing error. May be the source code has no '=' or ';' The source code has the mistake[s] in the syntax";
	}
	throw vk::exception("No information about gotten type", __FILE__, __FUNCTION__, __LINE__);
}
const wchar_t* __ObjectParserBase::getInfoAboutStatusW()const noexcept(false) {
	return __ObjectParserBase::getInfoAboutStatusW(last_status);
}
const char* __ObjectParserBase::getInfoAboutStatus(Status st)noexcept(false) {
	using vk::unsafe::ObjectParser;
	switch (st) {
		case Status::none:
		return "The status has not been set yet";
		case Status::ok:
		return "OK. No errors";
		case Status::no_such_file:
		return "A file not found or may be no such file";
		case Status::data_is_nullptr:
		return "Some transfered data are nullptr";
		case Status::key_is_nullptr:
		return "Key not found was found nullptr. May be some error with syntax";
		case Status::value_is_nullptr:
		return "Value not found was found nullptr. May be some error with syntax";
		case Status::key_parse_error:
		return "Key-parsing error. The source code has the mistake[s] in the syntax";
		case Status::value_parse_error:
		return "Value-parsing error. The source code has the mistake[s] in the syntax";
		case Status::token_is_uncorrect:
		return "Some-token-parsing error. May be the source code has no '=' or ';' The source code has the mistake[s] in the syntax";
	}
	throw vk::exception("No information about gotten type", __FILE__, __FUNCTION__, __LINE__);
}
const char* __ObjectParserBase::getInfoAboutStatus()const noexcept (false) {
	return ObjectParser::getInfoAboutStatus(last_status);
}
__ObjectParserBase::Status __ObjectParserBase::getStatus() const noexcept{
	return Status(last_status);
}
void __ObjectParserBase::clear() noexcept{
	last_status = Status::none;
}
__ObjectParserBase::__ObjectParserBase() noexcept{
	__init__();
}
__ObjectParserBase::~__ObjectParserBase(){
}
__ObjectParserBase::__ObjectParserBase(const __ObjectParserBase& obj) noexcept{
	*this = obj;
}
__ObjectParserBase::__ObjectParserBase(__ObjectParserBase&& obj) noexcept{
	*this = std::move(obj);
}
__ObjectParserBase& __ObjectParserBase::operator=(const __ObjectParserBase& obj) noexcept{
	last_status = obj.last_status;
	return *this;
}
__ObjectParserBase& __ObjectParserBase::operator=(__ObjectParserBase&& obj) noexcept{
	last_status = obj.last_status;
	obj.last_status = Status::none;
	return *this;
}

}