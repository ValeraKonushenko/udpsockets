#pragma once
#include "../namespaces_decl.h"
#include "../Container.h"
#include <vector>

/*
* ObjectParser - this class parser specified data in the strength format
* Format(rules):
*	-every token divided ';'. E.g: token_1;token_2;token_3;token_4;
*	-every token has to has key and value of it. E.g: name=Valera;
*	-every token' key can't to start from '\r' or '\n'
*	-if you want to put some text with banned symbols you should to use ``
*Banned sybols: = ;
* 
* 
* ============Tests==============
* Test#1
* Time of setting data: 188
* size(tokens): 1'000'000
* Time of getting data by a string: 4 
* 
* Test#2
* Time of setting data: 184
* size(tokens): 1'000'000
* Time of getting data by an index: 0
* 
* Test#3
* Time of setting data: 21
* size(tokens): 100'000
* Time of getting data by a string: 1
* 
* 
* 
* 
* 
* 
* Examples:
//=======SET THE STRING=========
ObjectParser p("var = val;");
p.setData("var = val;");


//=======GETTING VALUE=========
ObjectParser p("var = val;");
cout << p["var"]->data() << endl;			
cout << p.getValue("var")->data() << endl;	
cout << p[0]->data() << endl;			
cout << p.getValue(0)->data() << endl;	
//=>val
//=>val


//=======GET PAIR=========
ObjectParser p("var = val;");
ObjectParser::pair* pair = nullptr;
pair = p.getPair("var");
if (!pair) {
	cout << "No such pair\n";
} else {
	cout << "Key:   " << pair->key.data() << endl;
	cout << "Value: " << pair->value.data() << endl;
}
pair = p.getPair(0);
if (!pair) {
	cout << "No such pair\n";
} else {
	cout << "Key:   " << pair->key.data() << endl;
	cout << "Value: " << pair->value.data() << endl;
}
//=>Key:   var
//=>Value: val


//=======CHECK FOR A KEY=========
ObjectParser p("var = val;");
if (p.isKey("var")) {
	cout << "OK";
}
//=>OK


//=======GET IS EMPTY=========
ObjectParser p("var = val;");
if (p.isEmpty()) {
	cout << "empty";
}
//=>empty


//=======GET STATUS=========
ObjectParser p("var = val;");
cout << p.getInfoAboutStatus() << endl;
ObjectParser::Status staus = p.getStatus();
cout << ObjectParser::getInfoAboutStatus(staus);
//=>ok. No errors
//=>ok. No errors


//=======GET SIZE=========
ObjectParser p("var = val;");
cout << p.size() << endl;
//=>1


//=======ADDIN NEW VAR-VAL=========
ObjectParser p;
p << "var = val;" << "var1 = val;";
p << "var3 = val;var4 = val;";
if (p.push("name = Oleg") != ObjectParser::Status::ok) {
	cout << p.getInfoAboutStatus() << endl;
}


//=======WORKING WITH ANY STRING=========
ObjectParser p;
//p << "var = 2 + 3 = 5; 2 - 2 = 0"; - ERROR expected tokens: ; =
p << "var = `2 + 3 = 5; 2 - 2 = 0`";//All okay
cout << p["var"]->data() << endl;
//=>2 + 3 = 5; 2 - 2 = 0

//=======WORKING WITH ANY STRING=========
ObjectParser p;
p << "var = `The cite: /`he was dead.../` - said me Tom`";
cout << p["var"]->data() << endl;
//=>The cite: `he was dead...` - said me Tom


//=======GETTIN a STRING FORM ALL DATA=========
ObjectParser p;
p << "var = val;var1 = val1;var2 = val2;var3 = val3;";
cout << p.inspect().data() << endl;
//=>var = val;var1 = val1;var2 = val2;var3 = val3;

*/
VK_NAMESPACE__VK_UNSAFE__BEG
class __ObjectParserBase {
public:
	enum class Status {
		none,
		ok,
		no_such_file,
		data_is_nullptr,
		key_is_nullptr,
		value_is_nullptr,
		key_parse_error,
		value_parse_error,
		token_is_uncorrect
	};
private:
protected:
	virtual void __init__()noexcept;
	Status last_status;
public:
	static const wchar_t*	getInfoAboutStatusW(Status st)			noexcept(false);
	virtual const wchar_t*	getInfoAboutStatusW()					const noexcept(false);
	static const char*		getInfoAboutStatus(Status st)			noexcept(false);
	virtual const char*		getInfoAboutStatus()					const noexcept(false);
	Status					getStatus()								const noexcept;
	virtual void			clear()									noexcept;
	__ObjectParserBase()											noexcept;
	virtual ~__ObjectParserBase();
	__ObjectParserBase(const __ObjectParserBase& obj)				noexcept;
	__ObjectParserBase(__ObjectParserBase&& obj)					noexcept;
	__ObjectParserBase& operator=(const __ObjectParserBase& obj)	noexcept;
	__ObjectParserBase& operator=(__ObjectParserBase&& obj)			noexcept;
};




class ObjectParser : public __ObjectParserBase{
public:
	using Status = __ObjectParserBase::Status;
	typedef char								char_t;
	typedef Container<char_t>					key_t;
	typedef Container<char_t>					value_t;
	struct pair {
		key_t	key;
		value_t value;
	};
private:
	void pushToContainer(pair&p);
	bool isValidChar(char_t ch);
	int getLenOfWord(const char_t* p);
	const char_t* goToNextToken(const char_t* p);
	const char_t* getPointerToKey(const char_t* p);
	const char_t* getPointerToValue(const char_t* p);
	const char_t* parseKey(const char_t* pkey, key_t& cnt);
	const char_t* parseValue(const char_t* pkey, value_t& cnt);
protected:
	Status parser(const char_t* data) noexcept;
	virtual void __init__()noexcept;
	std::vector<pair> data;
public:
	Status				setDataFromFile	(const char_t* path)	noexcept;
	Status				setData			(const char_t* data)	noexcept;
	bool				isKey			(const char_t* key)		const noexcept;
	bool				isEmpty			()						const noexcept;
	value_t*			operator[]		(const char_t* key)		noexcept;
	value_t*			getValue		(const char_t* key)		noexcept;
	pair*				getPair			(const char_t* key)		noexcept;
	value_t*			operator[]		(size_t i)				noexcept;
	value_t*			getValue		(size_t i)				noexcept;
	pair*				getPair			(size_t i)				noexcept;
	value_t				operator[]		(const char_t* key)		const noexcept;
	value_t				getValue		(const char_t* key)		const noexcept;
	value_t				operator[]		(size_t i)				const noexcept;
	value_t				getValue		(size_t i)				const noexcept;
	virtual void		clear			()						noexcept;
	size_t				size			()						const noexcept;
	Status				push			(const char_t* key, const char_t* value)noexcept;
	Status				push			(const char_t* expr)	noexcept;
	ObjectParser&		operator<<		(const char_t* expr)	noexcept;
	Container<char_t>	inspect			(const char_t* after_tok = "")const noexcept(false);

	ObjectParser(const char_t* data = nullptr)					noexcept;
	virtual ~ObjectParser();
	ObjectParser(const ObjectParser& obj)						noexcept;
	ObjectParser(ObjectParser&& obj)							noexcept;
	ObjectParser& operator=(const ObjectParser& obj)			noexcept;
	ObjectParser& operator=(ObjectParser&& obj)					noexcept;
};





class ObjectParserW : public __ObjectParserBase {
public:
	using Status = __ObjectParserBase::Status;
	typedef wchar_t								char_t;
	typedef Container<char_t>					key_t;
	typedef Container<char_t>					value_t;
	struct pair {
		key_t	key;
		value_t value;
	};
private:
	void pushToContainer(pair&p);
	bool isValidChar(char_t ch);
	int getLenOfWord(const char_t* p);
	const char_t* goToNextToken(const char_t* p);
	const char_t* getPointerToKey(const char_t* p);
	const char_t* getPointerToValue(const char_t* p);
	const char_t* parseKey(const char_t* pkey, key_t& cnt);
	const char_t* parseValue(const char_t* pkey, value_t& cnt);
protected:
	Status parser(const char_t* data) noexcept;
	virtual void __init__()noexcept;
	std::vector<pair> data;
public:
	Status				setDataFromFile	(const char_t* path)	noexcept;
	Status				setData			(const char_t* data)	noexcept;
	bool				isKey			(const char_t* key)		const noexcept;
	bool				isEmpty			()						const noexcept;
	value_t*			operator[]		(const char_t* key)		noexcept;
	value_t*			getValue		(const char_t* key)		noexcept;
	pair*				getPair			(const char_t* key)		noexcept;
	value_t*			operator[]		(size_t i)				noexcept;
	value_t*			getValue		(size_t i)				noexcept;
	pair*				getPair			(size_t i)				noexcept;
	value_t				operator[]		(const char_t* key)		const noexcept;
	value_t				getValue(const char_t* key)		const noexcept;
	value_t				operator[]		(size_t i)				const noexcept;
	value_t				getValue(size_t i)				const noexcept;
	void				clear			()						noexcept;
	size_t				size			()						const noexcept;
	Status				push			(const char_t* expr)	noexcept;
	ObjectParserW&		operator<<		(const char_t* expr)	noexcept;
	Container<char_t>	inspect(const char_t* after_tok = L"")const noexcept(false);

	ObjectParserW(const char_t* data = nullptr)					noexcept;
	virtual ~ObjectParserW();
	ObjectParserW(const ObjectParserW& obj)						noexcept;
	ObjectParserW(ObjectParserW&& obj)							noexcept;
	ObjectParserW& operator=(const ObjectParserW& obj)			noexcept;
	ObjectParserW& operator=(ObjectParserW&& obj)				noexcept;
};

VK_NAMESPACE__VK_UNSAFE__END