#pragma once
#include "Logger.h"
#include "SocketConnection.h"
#include "_WSAData.h"
#include "namespaces_decl.h"
#include <vector>
#include <mutex>

VK_NAMESPACE__VK_SAFE__BEG
class Acceptor {
private:
protected:
	mutable std::recursive_mutex	recmut;
	std::vector<SocketConnection> vec;
public:
	void				accept		(SocketConnection sc);
	size_t				size		()						const;
	SocketConnection	operator[]	(int)					const noexcept;
	SocketConnection	end			()						const noexcept;
	void				dropConnection(int pos)					noexcept;
	bool				isAccept	(const SocketConnection& sc, timeval time) const noexcept(false);

	Acceptor()												=default;
	virtual ~Acceptor				()						=default;
	Acceptor						(const Acceptor&obj)	noexcept;
	Acceptor& operator=				(const Acceptor&obj)	noexcept;
	Acceptor						(Acceptor&&obj)			noexcept;
	Acceptor& operator=				(Acceptor&&obj)			noexcept;
};
VK_NAMESPACE__VK_SAFE__END



VK_NAMESPACE__VK_UNSAFE__BEG
class Acceptor {
private:
protected:
	std::vector<SocketConnection> vec;
public:
	void				accept		(SocketConnection sc);
	size_t				size		()						const;
	SocketConnection	operator[]	(int)					const noexcept;
	SocketConnection	end			()						const noexcept;
	bool				isAccept(const SocketConnection& sc, timeval time) const noexcept(false);

	Acceptor()												= default;
	virtual ~Acceptor				()						= default;
	Acceptor						(const Acceptor&obj)	noexcept;
	Acceptor& operator=				(const Acceptor&obj)	noexcept;
	Acceptor						(Acceptor&&obj)			noexcept;
	Acceptor& operator=				(Acceptor&&obj)			noexcept;
};
VK_NAMESPACE__VK_UNSAFE__END