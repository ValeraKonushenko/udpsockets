#pragma once
#include "namespaces_decl.h"
#include <string>
#include <mutex>
#include <ctime>
VK_NAMESPACE__VK__BEG

typedef int errorID;
typedef errorID errorIDbool;


class Log {
public:
	union Error {
		errorID			id_error;
		errorIDbool		is_error;
	}_error;
private:
	mutable std::recursive_mutex recmut;
	std::string			msg;
	time_t				log_time;
protected:
public:

	Log								(std::string msg = "", errorID eid = 0);
	void			clear			();
	void			setLog			(std::string msg, errorID eid = 0);
	std::string		message			()const;
	Error			error			()const;
	bool			isError			()const;
	time_t			getTimeUTC		()const;
	std::string		getTime			()const;


	Log								(const Log&lg);
	Log								(Log&&lg)noexcept;
	Log&			operator=		(const Log& lg)noexcept;
	Log&			operator=		(Log&&lg)noexcept;

	virtual ~Log	()	= default;
};

VK_NAMESPACE__VK__END