#pragma once
#include "namespaces_decl.h"
VK_NAMESPACE__VK__BEG
	unsigned int getAmoOfChar(const char*src, char ch)noexcept;
VK_NAMESPACE__VK__END