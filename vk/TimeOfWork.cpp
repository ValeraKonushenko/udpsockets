#include "TimeOfWork.h"
namespace vk::unsafe {
clock_t TimeOfWork::stop() noexcept{
	clock_t last = clock() - time;
	clear();
	return last;
}
clock_t TimeOfWork::start() noexcept{
	time = clock();
	return (time);
}
void TimeOfWork::clear() noexcept{
	time = 0l;
}
TimeOfWork::TimeOfWork(bool is_auto_start)noexcept {
	clear();
	if (is_auto_start)start();
}
}