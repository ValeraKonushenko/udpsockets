#include "IDCounter.h"



VK_NAMESPACE__VK_SAFE__BEG
IDCounter::id_t IDCounter::id_counter = 0;

IDCounter::id_t IDCounter::getNewID() noexcept(false){
	std::lock_guard lg(recmut);
	return id_counter++;
}

VK_NAMESPACE__VK_SAFE__END

