#pragma once
#include "namespaces_decl.h"
#include <Windows.h>
#include <locale>
#include <codecvt>
#include <string>


VK_NAMESPACE__VK__BEG
	std::string  utf8_encode(const std::wstring& src);
VK_NAMESPACE__VK__END