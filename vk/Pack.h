#pragma once
#include "namespaces_decl.h"
#include "Package.h"
#include <vector>
#include <mutex>

VK_NAMESPACE__VK_UDP_SAFE__BEG
class Pack {
public:
	typedef unsigned int size_t;
private:
protected:
	mutable std::recursive_mutex recmut;
	std::vector<Package>		 pack;
	size_t						 def_pack_size;
	size_t						 amo_of_packages;
	void			dataToPack	(size_t size, const char* data)	noexcept(false);
public:
	void			setBuffSize	(size_t size)					noexcept;
	size_t 			getBuffSize	()								const noexcept;
	size_t 			getAmoOfPackages()							const noexcept;
	bool			isEmpty		()								const noexcept;
	size_t			size		()								const noexcept;
	void			clear		()								noexcept;
	Package			get			(size_t i)						const noexcept(false);
	const Package&	getCRef		(size_t i)						const noexcept(false);
	void			set			(size_t i, const Package& el)	noexcept(false);
	void			set			(size_t i, const char* data)	noexcept(false);
					Pack		()								noexcept;
					~Pack		()								;
					Pack		(const Pack&obj)				noexcept(false);
					Pack		(Pack&&obj)						noexcept(false);
	Pack&			operator=	(const Pack&obj)				noexcept(false);
	Pack&			operator=	(Pack&&obj)						noexcept(false);
};
struct _PackData {
	Pack::size_t amo_packages;
	vk::safe::IDCounter::id_t beg;
	vk::safe::IDCounter::id_t end;
};
VK_NAMESPACE__VK_UDP_SAFE__END