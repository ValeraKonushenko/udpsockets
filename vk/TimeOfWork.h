#pragma once
#include "namespaces_decl.h"
#include <ctime>

VK_NAMESPACE__VK_UNSAFE__BEG
class TimeOfWork {
private:
protected:
	clock_t time;
public:
	clock_t stop()noexcept;
	clock_t start()noexcept;
	void clear()noexcept;
	TimeOfWork(bool is_auto_start = true)noexcept;
	~TimeOfWork()											= default;
	TimeOfWork(const TimeOfWork& obj)						= default;
	TimeOfWork(TimeOfWork&& obj)							= default;
	TimeOfWork& operator=(const TimeOfWork& obj)			= default;
	TimeOfWork& operator=(TimeOfWork&& obj)					= default;
};
VK_NAMESPACE__VK_UNSAFE__END