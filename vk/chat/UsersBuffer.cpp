#include "UsersBuffer.h"
#include <exception>
namespace vk::chat::safe {
bool UsersBuffer::findAndRemove(key_t i) noexcept{
	container::iterator it = users.find(i);
	if (it == users.end()) return false;
	users.erase(it);
	return true;
}
void UsersBuffer::push(User&user, SOCKET socket) noexcept(false){
	std::lock_guard lg(recmut);
	container::iterator it = users.find(user.id());
	if (it != users.end()) {
		throw std::exception("This user already exist");
	}
	value_t val = { user,socket };
	key_t key	= user.id();
	users.insert({ key, std::move(val) });
}
size_t UsersBuffer::size() const noexcept{
	std::lock_guard lg(recmut);
	return size_t(users.size());
}
bool UsersBuffer::isByKey(key_t i) const noexcept(false) {
	std::lock_guard lg(recmut);
	auto it = users.find(i);
	if (it == users.end())
		return false;
	return true;
}
const UsersBuffer::value_t& UsersBuffer::getByKey(key_t i) const noexcept(false){
	std::lock_guard lg(recmut);
	auto it = users.find(i);
	if (it == users.end())
		throw std::exception("No such user, or you try to went outside the container");
	return it->second;
}
const UsersBuffer::value_t& UsersBuffer::getByIndex(size_t i) const noexcept(false){
	auto it = users.begin();
	size_t _i = 0u;
	while (it != users.end()) {
		if (_i == i)return it->second;
		it++;
		_i++;
	}
	throw std::exception("You try to went outside the container");
}

const UsersBuffer::value_t& UsersBuffer::operator[](size_t i) const noexcept(false){
	return getByIndex(i);
}




UsersBuffer::UsersBuffer() noexcept {}
UsersBuffer::~UsersBuffer() {}
UsersBuffer::UsersBuffer(const UsersBuffer& obj) noexcept{
	std::scoped_lock lg(recmut, obj.recmut);
	*this = obj;
}
UsersBuffer::UsersBuffer(UsersBuffer&& obj) noexcept{
	std::scoped_lock lg(recmut, obj.recmut);
	*this = std::move(obj);
}
UsersBuffer& UsersBuffer::operator=(const UsersBuffer& obj) noexcept{
	std::scoped_lock lg(recmut, obj.recmut);
	return *this;
}
UsersBuffer& UsersBuffer::operator=(UsersBuffer&& obj) noexcept{
	std::scoped_lock lg(recmut, obj.recmut);
	return *this;
}
}