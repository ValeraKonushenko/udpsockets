//#include "pch.h"
#include "VKCP.h"
#include "../Assert.h"
#include "../CStringExt.h"
#include <cstring>
#include <stdio.h>
#include <string>
namespace vk::proto {
VKCP::ccharpc	VKCP::encoding			= "ASCII";
VKCP::cuint		VKCP::size				= 256u;
VKCP::cuint		VKCP::size_of_one_char	= sizeof(VKCP::char_t);
VKCP::cuint		VKCP::username_max_len	= 64u;
VKCP::cuint		VKCP::encoding_max_len	= 16u;
VKCP::cuint		VKCP::data_max_len		= 128u;
VKCP::cfloat	VKCP::version			= 1.0f;


const VKCP::char_t* VKCP::getExample() noexcept{
	return 
		"1.0\n"
		"ASCII\n"
		"333\n"
		"0\n"
		"1\n"
		"SomeUserName\n"
		"SomeData\n"
		;
}
const VKCP::char_t* VKCP::getExampleRules() noexcept {
	return
		"1: version\n"
		"2: encoding(head)\n"
		"3: body's size\n"
		"4: self ID\n"
		"5: connect ID\n"
		"6: username\n"
		"7: somedata\n"
		;
}

bool VKCP::checkProto(ccharp src) noexcept(false){
	static_assert(sizeof(char_t) == 1u, "cstring functions can't work with type of character greater then 1byte. Change please char_t type to equalent of C-char.");
	if (src == nullptr)
		throw std::exception("checkProto: Source is nullptr");
	if (strcmp(src, "") == 0)
		throw std::exception("checkProto: Source is empty");
	if (getAmoOfChar(getExampleRules(), '\n') != getAmoOfChar(src, '\n'))
		throw std::exception("checkProto: Source has different value of \\n");
	return true;
}

VKCP::VKCPHead VKCP::parse(ccharp src) noexcept(false){
	VKCP::VKCPHead head;
	auto goToNewLine = [](ccharp& p) {
		static_assert(sizeof(char_t) == 1u, "cstring functions can't work with type of character greater then 1byte. Change please char_t type to equalent of C-char.");
		while (p) {
			if (*p != '\0') {
				p++;
				if (*p == '\n') {
					p++;
					break;
				}
			} else {
				p = nullptr;
				break;
			}
		}
	};
	checkProto(src);
	static_assert(sizeof(char_t) == 1u, "cstring functions can't work with type of character greater then 1byte. Change please char_t type to equalent of C-char.");
	ccharp p = src;
	head.version = atof(p);
	goToNewLine(p);	
	size_t i = 0u;
	strncat_s(head.encoding, encoding_max_len, p,strchr(p,'\n') - p);
	goToNewLine(p);	
	head.body_size = static_cast<unsigned int>(atoi(p));
	goToNewLine(p);
	head.self_id = static_cast<unsigned int>(atoi(p));
	goToNewLine(p);
	head.connect_id = static_cast<unsigned int>(atoi(p));
	goToNewLine(p);
	strncat_s(head.username, VKCP::username_max_len, p, strchr(p, '\n') - p);
	goToNewLine(p);
	strncat_s(head.data, VKCP::data_max_len, p, strchr(p, '\n') - p);
	return head;
}

unsafe::Container<VKCP::char_t> VKCP::generate(
	cfloat version, 
	ccharp encoding, 
	cuint body_size,
	uint self_id,
	uint connect_id,
	ccharp username,
	ccharp data)
	noexcept(false){
	#pragma region CHECK_FOR_ERROR
	if (version <= 0.0f)
		throw std::exception("The version can't be less or equal then 0.0");
	if (isnan(version))
		throw std::exception("The version can't be NAN");
	if (isinf(version))
		throw std::exception("The version can't be inf");
	if (!encoding) 
		throw std::exception("Encoding can't be empty");
	if (!username)
		throw std::exception("Username is nullptr");
	if (strlen(username) >= username_max_len)
		throw std::exception(("Username is too long, max len: " + std::to_string(username_max_len)).c_str());
	if (data && strlen(data) >= data_max_len)
		throw std::exception(("Data is too long, max len: " + std::to_string(data_max_len)).c_str());
	#pragma endregion
	unsafe::Container<VKCP::char_t> head;
	try {
		head.reserve(size);
	} catch (const std::bad_alloc&e) {
		throw std::exception(e.what());
	}

	static_assert(sizeof(char_t) == 1u, "cstring functions can't work with type of character greater then 1byte. Change please char_t type to equalent of C-char.");
	char ss[12];
	sprintf_s(head, size, "%.1f", version);
	strcat_s(head, size, "\n");
	strcat_s(head, size, encoding);
	strcat_s(head, size, "\n");
	char* p = strrchr(head, '\n');
	_itoa_s(static_cast<int>(body_size), p + 1, size - (p - head) - 1, 10);
	strcat_s(head, size, "\n");
	p = strrchr(head, '\n');
	_itoa_s(static_cast<int>(self_id), p + 1, size - (p - head) - 1, 10);
	strcat_s(head, size, "\n");
	p = strrchr(head, '\n');
	_itoa_s(static_cast<int>(connect_id), p + 1, size - (p - head) - 1, 10);
	strcat_s(head, size, "\n");
	strcat_s(head, size, username);
	strcat_s(head, size, "\n");
	if(data)strcat_s(head, size, data);
	else    strcat_s(head, size, "none");
	strcat_s(head, size, "\n");
	return std::move(head);
}

unsafe::Container<VKCP::char_t> VKCP::generate(
	cuint body_size, 
	uint self_id, 
	uint connect_id,
	ccharp username,
	ccharp data) noexcept(false)
{	
	return std::move(VKCP::generate(
		version, encoding, body_size, self_id, connect_id, username, data));
}

VKCP::VKCPHead::VKCPHead()noexcept {
	this->username	= new VKCP::char_t[VKCP::username_max_len]{};
	this->data		= new VKCP::char_t[VKCP::data_max_len]{};
	this->encoding	= new VKCP::char_t[VKCP::encoding_max_len]{};
}
VKCP::VKCPHead::~VKCPHead(){
	delete[] this->username;
	delete[] this->data;
	delete[] this->encoding;
}

VKCP::VKCPHead::VKCPHead(const VKCPHead& obj)noexcept {
	*this = obj;
}
VKCP::VKCPHead::VKCPHead(VKCPHead&& obj)noexcept {
	*this = std::move(obj);
}
VKCP::VKCPHead& VKCP::VKCPHead::operator=(const VKCPHead& obj)noexcept {
	version		= obj.version;
	encoding	= new VKCP::char_t[VKCP::encoding_max_len];
	strcpy_s(encoding, VKCP::encoding_max_len, obj.encoding);
	body_size	= obj.body_size;
	self_id		= obj.self_id;
	connect_id	= obj.connect_id;
	username	= new VKCP::char_t[VKCP::username_max_len]{};
	strcpy_s(username, VKCP::username_max_len, obj.username);
	data		= new VKCP::char_t[VKCP::data_max_len]{};
	strcpy_s(data, VKCP::data_max_len, obj.data);
	return *this;
}
VKCP::VKCPHead& VKCP::VKCPHead::operator=(VKCPHead&& obj)noexcept {
	version = obj.version;
	encoding = obj.encoding;
	body_size = obj.body_size;
	self_id = obj.self_id;
	connect_id = obj.connect_id;
	username = obj.username;
	data = obj.data;
	memset(&obj, 0, sizeof(obj));
	return *this;
}

}