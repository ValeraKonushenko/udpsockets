#pragma once
#include "../namespaces_decl.h"
#include "../Assert.h"
VK_NAMESPACE__VK__BEG
class GUIException : std::runtime_error {
public:
	GUIException(const std::string& msg);
	virtual char const* what() const override;
};
VK_NAMESPACE__VK__END