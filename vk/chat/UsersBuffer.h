#pragma once
#include "User.h"
#include <map>
#include <WinSock2.h>
namespace vk::chat::safe {
class UsersBuffer {
public:
	typedef User::ID_t					key_t;
	typedef std::pair<User, SOCKET>		value_t;
	typedef std::map<key_t, value_t>	container;
private:
protected:
	mutable std::recursive_mutex recmut;
	container users;
public:
	bool			findAndRemove	(key_t i)		noexcept;
	void			push			(User&,SOCKET s)noexcept(false);
	size_t			size			()				const noexcept;
	bool			isByKey			(key_t i)		const noexcept(false);
	const value_t&	getByKey		(key_t i)		const noexcept(false);
	const value_t&	getByIndex		(size_t i)		const noexcept(false);
	const value_t&	operator[]		(size_t i)		const noexcept(false);

	UsersBuffer()									noexcept;
	~UsersBuffer();
	UsersBuffer(const UsersBuffer& obj)				noexcept;
	UsersBuffer(UsersBuffer&& obj)					noexcept;
	UsersBuffer& operator=(const UsersBuffer&obj)	noexcept;
	UsersBuffer& operator=(UsersBuffer&&obj)		noexcept;
};
}