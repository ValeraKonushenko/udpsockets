#include "User.h"

namespace vk::chat::safe {

const User::ID_t	User::invalid_id = -1;
const User::string	User::invalid_name = "";

void User::__init(){
	_name	= invalid_name;
	_id		= invalid_id;
}
User::string		User::name	()					const noexcept {
	std::lock_guard lg(recmut);
	return _name;
}
bool				User::name	(string str)		noexcept {
	std::lock_guard lg(recmut);
	if (str == invalid_name)return false;
	_name = str;
	return true;
}
User::ID_t			User::id	()					const noexcept {
	std::lock_guard lg(recmut);
	return _id;
}
bool				User::id	(ID_t nid)			noexcept {
	std::lock_guard lg(recmut);
	if (nid == invalid_id)return false;
	_id = nid;
	return true;
}

User::User(){
	__init();
}
User::~User(){}
User::User(const User& obj) noexcept{
	*this = obj;
}
User::User(User&& obj)noexcept{
	*this = std::move(obj);
}

User& User::operator=(const User& obj) noexcept{
	std::scoped_lock sl(recmut, obj.recmut);
	_name = obj._name;
	_id = obj._id;
	return *this;
}

User&				User::operator=(User&& obj) noexcept{
	std::scoped_lock sl(recmut, obj.recmut);
	_name = obj._name;
	_id = obj._id;
	obj.__init();
	return *this;
}

}