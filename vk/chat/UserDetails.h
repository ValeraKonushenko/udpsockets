#pragma once
#include "User.h"
#include "VKCP.h"
#include <WinSock2.h>
namespace vk::chat::safe {
class UserDetail {
private:
	User user;
	SOCKET socket;
protected:
public:
	
	UserDetail(User&& user, SOCKET socket);
	~UserDetail()									= default;
	UserDetail(const UserDetail& obj)				= delete;
	UserDetail(UserDetail&& obj)					= delete;
	UserDetail& operator=(const UserDetail& obj)	= delete;
	UserDetail& operator=(UserDetail&& obj)			= delete;
};
}