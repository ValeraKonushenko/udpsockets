#pragma once
#include "namespaces_decl.h"
VK_NAMESPACE__VK__BEG
template<class T>
class Destroyer final{
private:
	T* data;
public:
	Destroyer();
	void set(T* pointer)noexcept(false);
	~Destroyer();
};

template<class T>
Destroyer<T>::Destroyer(){
	data = nullptr;
}
template<class T>
inline void Destroyer<T>::set(T* pointer){
	if (!data)data = pointer;
}
template<class T>
Destroyer<T>::~Destroyer() {
	if(data)delete data;
}
VK_NAMESPACE__VK__END