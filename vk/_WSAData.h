#pragma once
//#define _WIN32_WINNT 0x501
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <exception>
#include <mutex>



class _WSAData {
private:
	class Destroyer {
	public:
		const bool * const is_init;
		Destroyer(const bool* const is_init);
		~Destroyer();
	};
	static WSADATA		wsaData;
	static bool			is_init;
	static std::mutex	mut;
	static Destroyer	destroyer;

	_WSAData() = default;
protected:
public:
	static WSADATA* startup();
	_WSAData(const _WSAData&) = delete;
	_WSAData& operator=(const _WSAData&) = delete;
	_WSAData(_WSAData&&) = delete;
	_WSAData& operator=(_WSAData&&) = delete;
};
