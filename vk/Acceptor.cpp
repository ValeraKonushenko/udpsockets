//#include "pch.h"
#include "Acceptor.h"
#include <thread>

namespace vk::safe{
void				Acceptor::accept	(SocketConnection sc) {
	using namespace std;
	SOCKADDR_IN from{};
	int fromlen = sizeof(from);
	SOCKET sfrom = 
		::accept(sc.socket(), reinterpret_cast<struct sockaddr*>(&from), &fromlen);

	SocketConnection tmp;
	tmp.port(sc.port());
	tmp.sockaddr(from);
	tmp.socket(sfrom);
	std::lock_guard lg(recmut);
	vec.push_back(tmp);
	safe::Logger::push(Log("Client with socket:" + std::to_string(sfrom) + " was connected!\n"));
}
size_t				Acceptor::size		()const{
	std::lock_guard lg(recmut);
	return this->vec.size();
}
SocketConnection	Acceptor::operator[](int pos)const noexcept {
	std::lock_guard lg(recmut);
	int sz = static_cast<int>(vec.size());
	while (pos >= sz && sz)		pos -= sz;
	while (pos < 0)				pos += sz;
	return vec[pos];
}
SocketConnection	Acceptor::end		() const noexcept{
	std::lock_guard lg(recmut);
	return vec[vec.size() - 1];
}
void Acceptor::dropConnection(int pos) noexcept{
	std::lock_guard lg(recmut);
	int sz = static_cast<int>(vec.size());
	while (pos >= sz && sz)		pos -= sz;
	while (pos < 0)				pos += sz;
	vec.erase(vec.begin() + pos);
}
bool Acceptor::isAccept(const SocketConnection& sc, timeval time) const  noexcept(false) {
	fd_set fd;
	FD_ZERO(&fd);
	FD_SET(sc.socket(), &fd);
	int select_res = select(sc.socket() + 1, &fd, nullptr, nullptr, &time);
	//vk::safe::Logger::push(std::to_string(select_res) + " socket: " + std::to_string(sc.socket()));
	if (select_res < 0 && (errno != EINTR))
		throw std::exception("Error with `select(...)`");
	if (FD_ISSET(sc.socket(), &fd)) {
		return true;
	}
	if (select_res == 0) 	return false;
	return false;
}
Acceptor::Acceptor(const Acceptor& obj)				noexcept{
	std::scoped_lock sl(recmut, obj.recmut);	
	*this = obj;
}
Acceptor&			Acceptor::operator=	(const Acceptor& obj)	noexcept{
	std::scoped_lock sl(recmut, obj.recmut);	
	this->vec = obj.vec;
	return *this;
}
Acceptor::Acceptor(Acceptor&& obj)					noexcept{
	std::scoped_lock sl(recmut, obj.recmut);
	*this = std::move(obj);
}
Acceptor&			Acceptor::operator=	(Acceptor&& obj)		noexcept{
	std::scoped_lock sl(recmut, obj.recmut);	
	this->vec = std::move(obj.vec);
	return *this;
}
}






namespace vk::unsafe{
void				Acceptor::accept(SocketConnection sc) {
	using namespace std;

	SOCKADDR_IN from{};
	int fromlen = sizeof(from);
	SOCKET sfrom = 
		::accept(sc.socket(), reinterpret_cast<struct sockaddr*>(&from), &fromlen);

	SocketConnection tmp;
	tmp.port(sc.port());
	tmp.sockaddr(from);
	tmp.socket(sfrom);
	vec.push_back(tmp);
	safe::Logger::push(Log("Client was connected!\n"));
}
size_t				Acceptor::size()const{
	return this->vec.size();
}
bool Acceptor::isAccept(const SocketConnection& sc, timeval time) const noexcept(false) {
	fd_set fd;
	FD_ZERO(&fd);
	FD_SET(sc.socket(), &fd);
	int select_res = select(sc.socket() + 1, &fd, nullptr, nullptr, &time);
	if (select_res < 0) 		throw std::exception("Error with `select(...)`");
	else if (select_res > 0)	return true;
	else if (select_res == 0) 	return false;
	return false;
}
SocketConnection	Acceptor::operator[](int pos)const noexcept {
	int sz = static_cast<int>(vec.size());
	while (pos >= sz && sz)		pos -= sz;
	while (pos < 0)				pos += sz;
	return vec[pos];
}
SocketConnection	Acceptor::end() const noexcept{
	return vec[vec.size() - 1];
}
Acceptor::Acceptor(const Acceptor& obj)				noexcept{
	*this = obj;
}
Acceptor&			Acceptor::operator=(const Acceptor& obj)	noexcept{
	this->vec = obj.vec;
	return *this;
}
Acceptor::Acceptor(Acceptor&& obj)					noexcept{
	*this = std::move(obj);
}
Acceptor&			Acceptor::operator=(Acceptor&& obj)		noexcept{
	this->vec = std::move(obj.vec);
	return *this;
}
}