#include "Mediator.h"
#include "../namespaces_decl.h"
/*
Example:

class Btn : public vk::safe::Component{
public:
	Btn(vk::safe::Mediator& md) :Component(md) {
	}
	void click() {
		notify(1);
	}
	void rclick() {
		notify(2);
	}
};

class App : public vk::safe::Mediator {
public:
	Btn btn1;
	Btn btn2;
	App() : btn1(*this), btn2(*this) {

	}
	virtual void notify(vk::safe::Component& comp, long long event)override {
		if (comp == btn1) {
			if(event == 1)
				cout << "btn1 - click\n";
			else if(event == 2)
				cout << "btn1 - rclick\n";
		}
		if (comp == btn2) {
			if (event == 1)
				cout << "btn2 - click\n";
			else if (event == 2)
				cout << "btn2 - rclick\n";
		}
	}
};
*/
VK_NAMESPACE__VK_SAFE__BEG
class Component {
private:
	Mediator* mediator;
protected:
public:
	bool operator==(const Component& obj)noexcept;
	bool operator!=(const Component& obj)noexcept;
	virtual void clear()noexcept;
	virtual void notify(long long event)noexcept;
	Component(Mediator&mediator)noexcept;
	virtual ~Component();
	Component(const Component&obj)noexcept;
	Component(Component&&obj)noexcept;
	Component& operator=(const Component& obj)noexcept;
	Component& operator=(Component&& obj)noexcept;
};
VK_NAMESPACE__VK_SAFE__END