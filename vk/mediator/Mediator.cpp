#include "Mediator.h"
#include <utility>
VK_NAMESPACE__VK_SAFE__BEG
Mediator::Mediator() noexcept{}
Mediator::~Mediator(){}
Mediator::Mediator(const Mediator& obj) noexcept{
	*this = obj;	
}
Mediator::Mediator(Mediator&& obj) noexcept{
	*this = std::move(obj);
}
Mediator& Mediator::operator=(const Mediator& obj) noexcept {
	return *this;
}
Mediator& Mediator::operator=(Mediator&& obj) noexcept{
	return *this;
}
VK_NAMESPACE__VK_SAFE__END