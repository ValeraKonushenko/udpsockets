#include "Component.h"
#include <utility>
VK_NAMESPACE__VK_SAFE__BEG
bool Component::operator==(const Component& obj) noexcept{
	return this == &obj;
}
bool Component::operator!=(const Component& obj) noexcept {
	return this == &obj;
}
void Component::clear() noexcept{
	this->mediator = nullptr;
}
void Component::notify(long long event) noexcept{
	mediator->notify(*this, event);
}
Component::Component(Mediator& mediator) noexcept{
	this->mediator = &mediator;
}
Component::~Component(){
	clear();
}
Component::Component(const Component& obj) noexcept{
	*this = obj;
}
Component::Component(Component&& obj) noexcept{
	*this = std::move(obj);
}
Component& Component::operator=(const Component& obj) noexcept{
	this->mediator = obj.mediator;
	return *this;
}
Component& Component::operator=(Component&& obj) noexcept{
	this->mediator = std::move(obj.mediator);
	return *this;
}
VK_NAMESPACE__VK_SAFE__END