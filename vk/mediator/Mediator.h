#pragma once
#include "../namespaces_decl.h"
VK_NAMESPACE__VK_SAFE__BEG
/*
Example:

class Btn : public vk::safe::Component{
public:
	Btn(vk::safe::Mediator& md) :Component(md) {
	}
	void click() {
		notify(1);
	}
	void rclick() {
		notify(2);
	}
};

class App : public vk::safe::Mediator {
public:
	Btn btn1;
	Btn btn2;
	App() : btn1(*this), btn2(*this) {

	}
	virtual void notify(vk::safe::Component& comp, long long event)override {
		if (comp == btn1) {
			if(event == 1)
				cout << "btn1 - click\n";
			else if(event == 2)
				cout << "btn1 - rclick\n";
		}
		if (comp == btn2) {
			if (event == 1)
				cout << "btn2 - click\n";
			else if (event == 2)
				cout << "btn2 - rclick\n";
		}
	}
};
*/
class Component;

class Mediator {
private:
protected:
public:
	virtual void notify(Component& comp, long long event) = 0;
	Mediator()noexcept;
	virtual ~Mediator();
	Mediator(const Mediator&obj)noexcept;
	Mediator(Mediator&&obj)noexcept;
	Mediator& operator=(const Mediator& obj)noexcept;
	Mediator& operator=(Mediator&& obj)noexcept;
};

VK_NAMESPACE__VK_SAFE__END
