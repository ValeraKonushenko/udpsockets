#include "Package.h"
#include "Assert.h"


VK_NAMESPACE__VK_UDP_SAFE__BEG

void Package::__init(){
	std::lock_guard lg(recmut);
	storage = nullptr;
	size	= 0;
	flag	= 0;
}
vk::safe::IDCounter::id_t Package::getId() const noexcept(false){
	return this->id;
}
vk::unsafe::Container<Package::byte>
Package::getData()const  noexcept(false){
	std::lock_guard lg(recmut);
	if (isEmpty())
		throw std::runtime_error("Inpossible get ID from empty package");
	vk::unsafe::Container<byte> c;
	c.set(storage, size);
	return std::move(c);
}
Package::byte Package::getFlag(const Package& p) noexcept(false){
	std::lock_guard lg(p.recmut);
	if (p.isEmpty())
		throw std::runtime_error("Inpossible get ID from empty package");
	return *reinterpret_cast<byte*>(p.storage + sizeof(vk::safe::IDCounter::id_t));
}
Package::Package(size_t sz) noexcept(false){
	__init();
	setBufferSize(sz);
}
bool Package::isEmpty() const noexcept(false){
	std::lock_guard lg(recmut);
	return storage == nullptr;
}
void Package::fill(byte el) noexcept(false){
	std::lock_guard lg(recmut);
	if (isEmpty())
		throw std::runtime_error("Inpossible fill empty package");
	memset(storage, el, size);
}
void Package::setBufferSize(size_t sz) noexcept(false){
	std::lock_guard lg(recmut);
	clear();
	size = sz;
	storage = new byte[size]{};
	if (storage == nullptr)
		throw vk::exception("Bad alloc", __FILE__,__FUNCTION__,__LINE__);
}
Package::size_t Package::getBufferSize() const noexcept(false){
	std::lock_guard lg(recmut);
	return size;
}
bool Package::Predicate::operator()(const Package& a, const Package& b) {
	return a.getId() < b.getId();
}
Package::size_t Package::getHeaderSize() const noexcept{
	return sizeof(id) + sizeof(flag);
}
Package::size_t Package::getFullSize() const noexcept{
	return getHeaderSize() + getBufferSize();
}
void Package::clear() noexcept{
	std::lock_guard lg(recmut);
	delete[]storage;
	__init();
}
Package::byte Package::get(size_t i) const noexcept(false){
	std::lock_guard lg(recmut);
	if (!storage)
		throw std::runtime_error("The Package's container is empty");
	if (i >= size)
		throw std::runtime_error("Too big value for the package's container");
	return storage[i];
}
void Package::set(size_t i, byte el) noexcept(false){
	std::lock_guard lg(recmut);
	if (!storage)
		throw std::runtime_error("The Package's container is empty");
	if (i >= size)
		throw std::runtime_error("Too big value for the package's container");
	storage[i] = el;
}
void Package::set(size_t size, const void* el) noexcept(false){
	std::lock_guard lg(recmut);
	if(size > this->size)
		setBufferSize(size);
	memcpy(storage, el, size);
}
void Package::setID(vk::safe::IDCounter::id_t id) noexcept{
	std::lock_guard lg(recmut);
	this->id = id;
}
void Package::setFlag(byte flag) noexcept{
	std::lock_guard lg(recmut);
	this->flag = flag;
}
Package::byte Package::getFlag() noexcept{
	std::lock_guard lg(recmut);
	return flag;
}
vk::unsafe::Container<Package::byte> Package::inspect() const noexcept(false){
	std::lock_guard lg(recmut);
	size_t sz = size + sizeof(id) + sizeof(flag);
	vk::unsafe::Container<byte> cont;
	byte* buff = new byte[sz]{};
	if (!buff)
		throw std::runtime_error("bad alloc");
	int skip = 0;
	memcpy(buff + skip, &id,sizeof(id));
	skip += sizeof(id);
	memcpy(buff + skip, &flag,sizeof(flag));
	skip += sizeof(flag);
	memcpy(buff + skip, storage, size);
	cont.set(&buff,sz);
// 0 1 2 3 4 5 6 7   8   9.........
//|1|0|0|0|0|0|0|0| |0| |0|0|0|0|0|0|
	return std::move(cont);
}
Package::~Package(){
	clear();
}
Package::Package(const Package& obj) noexcept{
	std::scoped_lock sl(recmut, obj.recmut);
	__init();
	*this = obj;
}
Package::Package(Package&& obj) noexcept{
	std::scoped_lock sl(recmut, obj.recmut);
	__init();
	*this = std::move(obj);
}
Package& Package::operator=(const Package& obj) noexcept{
	std::scoped_lock sl(recmut, obj.recmut);
	setBufferSize(obj.size);
	this->id = obj.id;
	this->flag = obj.flag;
	memcpy(storage, obj.storage, size);
	return *this;
}
Package& Package::operator=(Package&& obj) noexcept{
	std::scoped_lock sl(recmut, obj.recmut);
	storage = obj.storage;
	size = obj.size;
	this->id = obj.id;
	this->flag = obj.flag;
	obj.__init();
	return *this;
}
VK_NAMESPACE__VK_UDP_SAFE__END
