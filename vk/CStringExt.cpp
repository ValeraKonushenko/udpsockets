#include "CStringExt.h"
namespace vk {

unsigned int getAmoOfChar(const char* src, char ch) noexcept {
    if (!src)        return 0;
    if (ch == '\0')  return 0;
    unsigned int amo = 0u;
    for (unsigned int i = 0; src[i]; i++) {
        if (src[i] == ch)amo++;
    }
    return amo;
}

}