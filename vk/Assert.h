#pragma once
#include <typeinfo>
#include <string>
#include <string_view>
#include <exception>
#include <stdexcept>
/*
* DEPRECATED
#define GET_ASSERT_MSG(msg) \
		("Error: at " + std::to_string(__LINE__) + " line\nIn the file: " \
		+ __FILE__ + "\nIn the function: " + __FUNCTION__ + "\n\nDescription: "\
		+ std::string(msg))
#define GET_ASSERT_MSG(class_name,msg) \
		("Error: at " + std::to_string(__LINE__) + " line\nIn the file: " \
		+ __FILE__ + "\nIn the class: " + typeid(class_name).name() + \
		"\nIn the function: " + __FUNCTION__ + "\n\nDescription: "\
		+ std::string(msg))
#define GET_ENV_INF_MSG(msg) \
		(msg + std::string(" at ") + std::to_string(__LINE__) + "line: " + \
		 __FUNCTION__)



#define GET_ENV_INF_MSG_C(msg) GET_ENV_INF_MSG(msg).c_str()
#define GET_ASSERT_MSG_C(class_name,msg) GET_ASSERT_MSG(class_name,msg).c_str()
#define GET_ASSERT_MSG_C(msg) GET_ASSERT_MSG(msg).c_str()

#define CREATE_EXCEPTION(class_name, msg) std::exception(GET_ASSERT_MSG_C(class_name, msg))
#define CREATE_EXCEPTION(msg) std::exception(GET_ASSERT_MSG_C(msg))
*/
namespace vk {
#define VK_MKEXCEPTION(message)vk::exception(message, __FILE__, __FUNCTION__, __LINE__)
	std::runtime_error exception(std::string_view message, std::string_view file, std::string_view func, long long line);
}