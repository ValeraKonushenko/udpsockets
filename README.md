# -------UDPSockets-------

### Introduction
This project demostrates the working of UDP connection client-client
Based on vk-library

### The targets
The main target is sending some messages to another client. You can send just text message any length, or 
you can send a .bmp image which will be displayed in the window

### How it works?
The logic of data sending:
I created class UDPSocket for storing and controling main processes like: sending, receiving, waiting for something.
The main logic arranged by means of client-client communication. So, you have no 'central' link like server. All data will come directyly.
All data break into:
1. Big data - class Pack
2. Smaller data - class Package
3. Simple Unit - just 1 byte of data

### How to send some Data?
-You can send any data(like bytes) with different length using this function:
	bool sendto(Address adr, const char* data, int size)const noexcept(false);
But I really don't reccoment use this method for complex data. You can use it for implement of own logic of data sending.

-Second case - using Package. Package is a wrapper of custom sending of data.
Using package you can send specified size of any data and get it like package.
The packages can't define whether it was delivered or not. You just send the
package and it can come or, may be, no. 
Also, you can't send data any size, so a package has a limit data' size
You should know about it: when you use Package, and size for it, this doesn't 
take into accout the size of header's part. The header's part also has a size,
is ID+flag. Now it take sizes of: unigned long long + char
	
-Third case - you can use a Pack. Pack is set of packages. You can transfer data
of any value using it, so a Pack break to piaces for sending like one package
Advantages:
- to control of recieving of data
- sending any size data, this splits the data by packages on its own
- getting of data happens in the right order
- reported the sender about data delivery(delivered\lost)


### How to reciev some Data?
You should implement of logic and agreements of data exchange. 
For example I reccoment you use the Pack for sending and reciev some data like Package.
Every package consist from ID(unsigned long long), some flags(1 byte), and your data buffer(you can set the most compatible size for you and use it)


### Main unit - Package
This part of all logic the most important, so using it you can exchange data as you wish
ID - you can use for marking of your data.
FLAGS - your own flags for unit
Buffer - this part of a package stores your data. You can control package's size using Package::setBuffSize(size_t size)

+--+-----+-------------------------+  
|ID|FLAGS|Buffer with custom length| - sum length of this will calculate like: sizeof(ID) + sizeof(FLAGS) + buff_size  
+--+-----+-------------------------+  



# Own comments
This demonstrated project my first experience of working with UPD. Now I got a lot of valuable experience with creating some things like it.
In the near future I wanna implement second version with considering all bad moments:
-Receiving data happens outside of class UPDSocket. I wanna fix it.
-I have no clear consistency with sending and recieving some data. I wanna create Wrapper of some class like UDPSocket and implement own porotocol with getting\putting data in clear format like one class Pack.