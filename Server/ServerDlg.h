#pragma once
#include "ServerModel.h"
#include "../vk/codeArchitecture/Settings.h"
#include "../vk/Logger.h"


class CServerDlg : public CDialogEx{
public:
	CServerDlg(CWnd* pParent = nullptr);
	CString getPort()const noexcept;
	CString getHost()const noexcept;
	CString getPortTo()const noexcept;
	CString getHostTo()const noexcept;

	//----BUTTONS-----
	void onStartServer();
	void onStopServer();
	void onClearLogs();
	void onClearMsg();
	void onStartServerTo();
	void onStopServerTo();
	void onSendMessage();
	void onSendFile();
	void putPhoto(CString path);
protected:
	


	void setPort(const CString& val)noexcept;
	void setHost(const CString& val)noexcept;
	void setPortTo(const CString& val)noexcept;
	void setHostTo(const CString& val)noexcept;
	void OnClose();
	virtual void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnInitDialog(); 
	CListBox *list_logger;
	CListBox * list_msg;
	vk::safe::Subscriber	sbscr_logger;
	vk::safe::Subscriber	sbscr_msg;
	vk::safe::Subscriber	sbscr_bmp;
	vk::safe::Settings		settings;
	ServerModel				model;
	HICON					m_hIcon;

	DECLARE_MESSAGE_MAP()
};
