#include "ServerModel.h"
#include "VKUP.h"
#include "../vk/CodeConvert.h"
#include "../vk/Assert.h"
#include <string>
#include <fstream>
#include <windows.h>





vk::unsafe::Container<char> ServerModel::getFileContentB(std::wstring_view path)
														const noexcept(false) {
	using namespace std;
	fstream file;
	vk::unsafe::Container<char> out;
	file.open(path.data(), ios::in | ios::binary);
	if (!file.is_open())
		throw std::runtime_error("No such file: " + string(path.begin(),path.end()));

	char* buff = nullptr;
	size_t fsize = 0u;
	file.seekg(0, ios::end);
	fsize = file.tellg();
	file.seekg(0, ios::beg);
	buff = new char[fsize];
	file.read(buff, fsize);
	out.set(&buff, fsize);

	file.close();
	return std::move(out);
}

void ServerModel::goRecData() noexcept(false){
	using namespace vk::UDP::safe;
	using namespace std;
	using namespace vk::safe;

	Address adr;
	Package package(VKUP::package_size);
	_PackData header;
	bool	wait_for_body = false;
	package.fill(0);
	while (is_run) {

		try {
			int rec = server.recvfrom(adr, package);
			if (rec != -1) {
				ifDataWasGot(adr, package, header, wait_for_body, rec);
				package.fill(0);
			}
		} catch (const std::runtime_error& ex) {
			Logger::push("Error while receiving of data: " + std::string(ex.what()));
		} catch (const nlohmann::json::type_error& ex) {
			Logger::push("Error while parsing JSON data: " + std::string(ex.what()));
		}
	}
}
void ServerModel::ifDataWasGot(Address& adr,
							   Package& package,
							   _PackData& header,
							   bool& wait_for_body,
							   int recieved_b) noexcept(false){
	using namespace vk::UDP::safe;
	using namespace std;
	using namespace vk::safe;


	static std::vector<Package> pack_data;
	static int last_loading = 0;
	static std::vector<char> long_data;

	switch (package.getFlag()) {
		case Package::Flag::header:{
			Package answer(VKUP::package_size);
			header = *reinterpret_cast<_PackData*>(package.getData().data());
			answer.setFlag(Package::Flag::answer);
			bool t = true;
			answer.set(sizeof(t), &t);
			server.sendto(adr, answer);

			last_loading = 0;
			pack_data.clear();
			long_data.clear();

			break;
		}
		case Package::Flag::body:{
			//if (!wait_for_body) {
			//	pack_data.clear();
			//	throw std::runtime_error("Received data was not expected");
			//}
			if (package.getId() < header.beg || package.getId() > header.end) {
				pack_data.clear();
				throw std::runtime_error("The package's ID is incorrect");
			}

			if (package.getId() == header.beg) {
				pack_data.reserve(header.amo_packages);
			}


			pack_data.push_back(package);


			if (package.getId() == header.end) {
				static std::vector<char> long_data;
				Package::Predicate cmp_less;
				std::sort(pack_data.begin(), pack_data.end(), cmp_less);

				long_data.reserve(header.amo_packages * package.getBufferSize());
				for (size_t i = 0; i < header.amo_packages; i++) {
					auto tmp = pack_data[i].getData();
					//std::fstream file;
					//file.open("out.txt", ios::app);
					//file.write(tmp.data(), tmp.getSize());
					//file.close();
					for (size_t j = 0; j < package.getBufferSize(); j++) {
						long_data.push_back(tmp.data()[j]);
					}
				}
				nlohmann::json json = nlohmann::json::parse(
					long_data.begin(), long_data.end());
				if (json.find("message") != json.end()) {
					onGotMessage(adr, package, json, recieved_b);
				} else if (json.find("action") != json.end()) {
					onGotAction(adr, package, json, recieved_b);
				}
				pack_data.clear();
				long_data.clear();
				sendAnswerB(adr, UDPSocket::data_was_got);
				//wait_for_body = false;
			}



			break;
		}
		case Package::Flag::body_bmp:
		{
			//if (!wait_for_body) {
			//	pack_data.clear();
			//	throw std::runtime_error("Received data was not expected");
			//}
			if (package.getId() < header.beg || package.getId() > header.end) {
				pack_data.clear();
				throw std::runtime_error("The package's ID is incorrect");
			}

			if (package.getId() == header.beg) {
				pack_data.reserve(header.amo_packages);
			}


			pack_data.push_back(package);
			int loading = static_cast<int>(1.0*pack_data.size() / header.amo_packages * 100.);
			if (loading - last_loading >= 10) {
				Logger::push("Was got " + std::to_string(loading) + "%");
				last_loading = loading;
			}
			

			if (package.getId() == header.end) {
				try {
					static std::string path = "photo.bmp";
					std::fstream file(path, std::ios::out);
					file.close();

					Package::Predicate cmp_less;
					std::sort(pack_data.begin(), pack_data.end(), cmp_less);
					if (pack_data.size() != header.amo_packages) {
						sendAnswerB(adr, UDPSocket::not_full_pack_of_package);
						throw std::runtime_error("Some packages were lost. Wait for resending.");
					}
				

					long_data.reserve(header.amo_packages * package.getBufferSize());
					vk::safe::Logger::push("Receiving was successfull");
					vk::safe::Logger::push("Saving...");
					int percent, last_percent = 0;
					for (size_t i = 0; i < header.amo_packages; i++) {
						auto tmp = pack_data[i].getData();
						file.open(path, ios::app | ios::binary);
						file.write(tmp.data(), tmp.getSize());
						file.close();
						percent = static_cast<int>(1. * i / header.amo_packages * 100.0);
						if (percent - last_percent >= 10) {
							vk::safe::Logger::push("Saved " + std::to_string(percent) + "%");
							last_percent = percent;
						}
					}
					path_to_photo = path;
					Logger::push("Photo was saved at: /"+ path);
					sendAnswerB(adr, UDPSocket::data_was_got);
					events.notify(gotBMP);
				} catch (const std::runtime_error&ex) {
					pack_data.clear();
					long_data.clear();
					//wait_for_body = false;
					throw std::runtime_error(ex);
				}
				pack_data.clear();
				long_data.clear();
				//wait_for_body = false;
			}



			break;
		}
		case Package::Flag::answer:{
			throw std::runtime_error("Data should was getting inside the socket");
			break;
		}
		default:{
			nlohmann::json json = nlohmann::json::parse((package.getData().data()));
			onGotUnknow(adr, package, json, recieved_b);
		break;
		}
	}
}

void ServerModel::onGotMessage(
			const Address& adr, 
			const Package& p, 
			const nlohmann::json& json,
			int recieved_b) noexcept(false){
	using namespace vk::UDP::safe;
	using namespace std;
	using namespace vk::safe;
	auto res = json["message"];
	std::wstring str(res.begin(), res.end());
	msg.push_back(str);
	events.notify(Event::gotChatMessage);
	//MessageBoxW(nullptr, str.c_str(), L"message", 1);
}

void ServerModel::onGotAction(const Address& adr, 
							  const Package& p, 
							  const nlohmann::json& json,
							  int recieved_b) noexcept(false){
	using namespace vk::UDP::safe;
	using namespace std;
	using namespace vk::safe;
	std::string f = json["action"];
	Logger::push(
		"Action from: " + adr.getFullAddress() + " was got: " +
		f
	);
}

void ServerModel::onGotUnknow(const Address& adr, 
							  const Package& p, 
							  const nlohmann::json& json,
							  int recieved_b) noexcept(false){
	using namespace vk::UDP::safe;
	using namespace std;
	using namespace vk::safe;
	Logger::push(
		"Got: " + to_string(recieved_b) + "byte. "
		"Expected: " + to_string(p.getBufferSize()) + "byte. "
		"Raw data: " + p.getData().data()
	);
}

void ServerModel::sendAnswerB(const Address& adr, int flag) noexcept(false){
	Package p(VKUP::package_size);
	p.setFlag(Package::answer);
	p.set(sizeof(flag), &flag);
	server.sendto(adr, p);
}

void ServerModel::openServer(std::wstring port) noexcept(false){
	server.open(_wtoi(port.c_str()));
	server.setNonBlock(1);
}
void ServerModel::closeServer() noexcept(false){
	server.close();
}

void ServerModel::startRecvData() noexcept{
	threads.push(new std::thread([this]() {		
		try {
			goRecData();
		}
		catch (const std::runtime_error&ex) {
			vk::safe::Logger::push("FATAL Error while receiving of data: " + std::string(ex.what()));
		}

	}));
}

void ServerModel::sendBmp(u_long port, const wchar_t* host, std::wstring path) noexcept(false){
	using namespace vk::UDP::safe;
	Address addr;
	addr.set(port, host);
	Pack pack;
	auto file = getFileContentB(path);
	pack.setBuffSize(VKUP::package_size);
	pack.set(file.getSize(), file.data());
	if (!server.sendto(addr, pack, Package::Flag::body_bmp))
		throw VK_MKEXCEPTION("Was sent not full pack of data. It may result in a error");
	vk::safe::Logger::push("Sending was successfull");
}

void ServerModel::sendStart(u_long port, const wchar_t*host) noexcept(false){
	using namespace vk::UDP::safe;
	Address addr;
	addr.set(port, host);
	Pack pack; 
	pack.setBuffSize(VKUP::package_size);
	nlohmann::json json;
	json["action"] = "start";
	auto dump = json.dump();
	pack.set(dump.size(),dump.c_str());
	if (!server.sendto(addr, pack))
		throw VK_MKEXCEPTION("Was sent not full pack of data. It may result in a error");
}

void ServerModel::sendMessage(u_long port, const wchar_t* host, std::wstring msg)
noexcept(false){
	using namespace vk::UDP::safe;
	Address addr;
	addr.set(port, host);
	Pack pack;
	pack.setBuffSize(VKUP::package_size);
	nlohmann::json json;
	//json["message"] = "Pellentesque sit amet sodales mauris, eu vestibulum velit. Pellentesque euismod ante eget vestibulum cursus. Vestibulum congue massa vel lacinia euismod. Ut ac nulla a erat feugiat aliquam. Aenean nec lectus massa. Pellentesque ut efficitur nisi. Etiam imperdiet elit biam.Character Counter is a 100 % free online character count calculator that's simple to use. Sometimes users prefer simplicity over all of the detailed writing information Word Counter provides, and this is exactly what this tool offers. It displays character count and word count which is often the only information a person needs to know about their writing. Best of all, you receive the needed information at a lightning fast speed.To find out the word and character count of your writing, simply copyand paste text into the tool or write directly into the text area.Once done, the free online tool will display both counts for the text that's been inserted. This can be useful in many instances, but it can be especially helpfulCharacter and word limits are quite common these days on the Internet.The one that most people are likely aware of is the 140 character limit for tweets on Twitter, but character limitsCharacterand word limits are quite common these days on the Internet.The one that most people are likely aware of is the 140 character limit for tweets on Twitter, but character limits";
	json["message"] = msg;
	auto dump = json.dump();
	pack.set(dump.size(), dump.c_str());
	if (!server.sendto(addr, pack))
		throw VK_MKEXCEPTION("Was sent not full pack of data. It may result in a error");
}

ServerModel::ServerModel() noexcept(false){
	is_run = true;
}
ServerModel::~ServerModel(){
	is_run = false;
}


