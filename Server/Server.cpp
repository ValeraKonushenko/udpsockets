#include "pch.h"
#include "framework.h"
#include "Server.h"
#include "ServerDlg.h"



BEGIN_MESSAGE_MAP(CServerApp, CWinApp)
END_MESSAGE_MAP()



CServerApp::CServerApp(){}
CServerApp theApp;

BOOL CServerApp::InitInstance(){
	CWinApp::InitInstance();

	CShellManager *pShellManager = new CShellManager;

	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));

	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	CServerDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (pShellManager != nullptr)
		delete pShellManager;

	return FALSE;
}

