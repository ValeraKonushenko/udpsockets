#include "pch.h"
#include "framework.h"
#include "Server.h"
#include "ServerDlg.h"
#include "afxdialogex.h"
#include "../vk/CodeConvert.h"
#include "../vk/Package.h"
#include "json.h"
#include "afxctl.h"
BEGIN_MESSAGE_MAP(CServerDlg, CDialogEx)
	ON_WM_CLOSE()
	ON_COMMAND(IDC_BUTTON_START, &CServerDlg::onStartServer)
	ON_COMMAND(IDC_BUTTON_STOP, &CServerDlg::onStopServer)
	ON_COMMAND(IDC_BUTTON_CLEAR_LOGS, &CServerDlg::onClearLogs)
	ON_COMMAND(IDC_BUTTON_CLEAR_MSG, &CServerDlg::onClearMsg)
	ON_COMMAND(IDC_BUTTON_START_TO, &CServerDlg::onStartServerTo)
	ON_COMMAND(IDC_BUTTON_STOP_TO, &CServerDlg::onStopServerTo)
	ON_COMMAND(IDC_BUTTON_SEND_MSG, &CServerDlg::onSendMessage)
	ON_COMMAND(IDC_BUTTON_SEND_FILE, &CServerDlg::onSendFile)
END_MESSAGE_MAP()

CServerDlg::CServerDlg(CWnd* pParent)
	: CDialogEx(IDD_SERVER_DIALOG, pParent){
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CString CServerDlg::getPort() const noexcept{
	CString str;
	GetDlgItemText(IDC_EDIT_PORT, str);
	return str;
}
CString CServerDlg::getHost() const noexcept{
	CString str;
	GetDlgItemText(IDC_EDIT_HOST, str);
	return str;
}
CString CServerDlg::getPortTo() const noexcept{
	CString str;
	GetDlgItemText(IDC_EDIT_PORT_TO, str);
	return str;
}
CString CServerDlg::getHostTo() const noexcept{
	CString str;
	GetDlgItemText(IDC_EDIT_HOST_TO, str);
	return str;
}
void CServerDlg::onStartServer(){
	try {
		model.openServer(getPort().GetString());
		model.startRecvData();
		GetDlgItem(IDC_BUTTON_SEND_FILE)->EnableWindow(0);
		GetDlgItem(IDC_BUTTON_SEND_MSG)->EnableWindow(0);
		GetDlgItem(IDC_BUTTON_START)->EnableWindow(0);
		GetDlgItem(IDC_BUTTON_STOP)->EnableWindow(1);
		GetDlgItem(IDC_BUTTON_START_TO)->EnableWindow(1);
		GetDlgItem(IDC_BUTTON_STOP_TO)->EnableWindow(0);
		GetDlgItem(IDC_EDIT_HOST)->EnableWindow(0);
		GetDlgItem(IDC_EDIT_PORT)->EnableWindow(0);
		GetDlgItem(IDC_EDIT_HOST_TO)->EnableWindow(0);
		GetDlgItem(IDC_EDIT_PORT_TO)->EnableWindow(1);
	} catch (const std::runtime_error&ex) {
		MessageBoxA(m_hWnd, ex.what(), "Fatal socket error", MB_ICONEXCLAMATION);
	}
}
void CServerDlg::onStopServer(){
	model.closeServer();
	GetDlgItem(IDC_BUTTON_SEND_FILE)->EnableWindow(0);
	GetDlgItem(IDC_BUTTON_SEND_MSG)->EnableWindow(0);
	GetDlgItem(IDC_BUTTON_START)->EnableWindow(1);
	GetDlgItem(IDC_BUTTON_STOP)->EnableWindow(0);
	GetDlgItem(IDC_BUTTON_START_TO)->EnableWindow(0);
	GetDlgItem(IDC_BUTTON_STOP_TO)->EnableWindow(0);
	GetDlgItem(IDC_EDIT_HOST)->EnableWindow(0);
	GetDlgItem(IDC_EDIT_PORT)->EnableWindow(1);
	GetDlgItem(IDC_EDIT_HOST_TO)->EnableWindow(0);
	GetDlgItem(IDC_EDIT_PORT_TO)->EnableWindow(0);
}
void CServerDlg::onClearLogs(){
	list_logger->ResetContent();
}
void CServerDlg::onClearMsg(){
	list_msg->ResetContent();
}
void CServerDlg::onStartServerTo(){
	vk::safe::Logger::push("Try to connect to client: " + std::string() +
	vk::utf8_encode((getHostTo()+L":"+getPortTo()).GetString()).c_str());
	GetDlgItem(IDC_BUTTON_START_TO)->EnableWindow(0);
	GetDlgItem(IDC_BUTTON_STOP_TO)->EnableWindow(1);
	GetDlgItem(IDC_BUTTON_SEND_FILE)->EnableWindow(1);
	GetDlgItem(IDC_BUTTON_SEND_MSG)->EnableWindow(1);
	try {		
		model.sendStart(_wtoi(getPortTo()), getHostTo());
	} catch (const std::runtime_error&ex) {
		vk::safe::Logger::push(std::string("FATAL error occurred during connecting to client: ")+ex.what());
	}
}
void CServerDlg::onStopServerTo(){
	vk::safe::Logger::push("Abrot conncection to client: " + std::string() +
						   vk::utf8_encode((getHostTo() + L":" + getPortTo()).GetString()).c_str());
	GetDlgItem(IDC_BUTTON_SEND_FILE)->EnableWindow(1);
	GetDlgItem(IDC_BUTTON_SEND_MSG)->EnableWindow(1);
	GetDlgItem(IDC_BUTTON_START_TO)->EnableWindow(1);
	GetDlgItem(IDC_BUTTON_STOP_TO)->EnableWindow(0);
}
void CServerDlg::onSendMessage(){
	CString msg;
	GetDlgItemText(IDC_EDIT_MSG, msg);
	try {
		list_msg->AddString(L"Outcome: "+ msg);
		model.sendMessage(_wtoi(getPortTo()), getHostTo(), msg.GetString());
	} catch (const std::runtime_error& ex) {
		vk::safe::Logger::push(std::string("Error occurred during connecting to client: ") + ex.what());
	}
}
void CServerDlg::onSendFile(){
	try {
		CFileDialog dia(1, nullptr, L"*.bmp", OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR);
		if (dia.DoModal() != IDOK) return;
		CString path = dia.GetPathName();
		list_msg->AddString(L"Outcome file: " + path);
		model.sendBmp(_wtoi(getPortTo()), getHostTo(), path.GetString());
	} catch (const std::runtime_error& ex) {
		vk::safe::Logger::push(std::string("Error occurred during file sending to client: ") + ex.what());
	}
}



void CServerDlg::putPhoto(CString path){
	CImage main_img;
	if (main_img.Load(path)) {
		vk::safe::Logger::push("Image can't be added");
		return;
	}
	CRect rect;
	GetDlgItem(IDC_OUTPUT_BMP)->GetWindowRect(&rect);

	CDC*dc = GetDlgItem(IDC_OUTPUT_BMP)->GetDC();
	CDC dc_img;
	dc_img.CreateCompatibleDC(dc);
	dc_img.SelectObject(main_img);
	dc->SetStretchBltMode(HALFTONE);
	dc->StretchBlt(0, 0, rect.Width(), rect.Height(),
				   &dc_img, 0, 0, main_img.GetWidth(), main_img.GetHeight(), SRCCOPY);

	
}


void CServerDlg::setPort(const CString& val) noexcept{
	GetDlgItem(IDC_EDIT_PORT)->SetWindowTextW(val);
}
void CServerDlg::setHost(const CString& val) noexcept{
	GetDlgItem(IDC_EDIT_HOST)->SetWindowTextW(val);
}
void CServerDlg::setPortTo(const CString& val) noexcept {
	GetDlgItem(IDC_EDIT_PORT_TO)->SetWindowTextW(val);
}
void CServerDlg::setHostTo(const CString& val) noexcept {
	GetDlgItem(IDC_EDIT_HOST_TO)->SetWindowTextW(val);
}
void CServerDlg::OnClose(){
	try {
		settings.setProperty("host", vk::utf8_encode(std::wstring(getHost().GetString())).c_str());
		settings.setProperty("port", vk::utf8_encode(std::wstring(getPort().GetString())).c_str());
		settings.setProperty("host_to", vk::utf8_encode(std::wstring(getHostTo().GetString())).c_str());
		settings.setProperty("port_to", vk::utf8_encode(std::wstring(getPortTo().GetString())).c_str());
		settings.saveSettings();
		model.is_run = false;
	} catch (const std::runtime_error&ex) {
		MessageBoxA(m_hWnd, ex.what(), "Settings saving error", MB_ICONEXCLAMATION);
	}
	onStopServer();
	CDialogEx::OnClose();
}
void CServerDlg::DoDataExchange(CDataExchange* pDX){
	CDialogEx::DoDataExchange(pDX);
}


BOOL CServerDlg::OnInitDialog(){
	CDialogEx::OnInitDialog();
	SetWindowText(L"Client");
	SetIcon(m_hIcon, TRUE);			
	SetIcon(m_hIcon, FALSE);	
	setHost(L"127.0.0.1");
	SetWindowLong(m_hWnd, GWL_STYLE, GetWindowLong(this->m_hWnd, GWL_STYLE)
				  | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_SYSMENU);
	onStopServer();

	//SETTINGS
	try {
		settings.setPath("last_data.dat").loadSettings();
		setPort(CString(settings["port"].data()));
		setHost(CString(settings["host"].data()));
		setPortTo(CString(settings["port_to"].data()));
		setHostTo(CString(settings["host_to"].data()));
	} catch (const std::runtime_error& ex) {
		MessageBoxA(m_hWnd, ex.what(), "Settings error", MB_ICONEXCLAMATION);
	}

	list_logger = reinterpret_cast<CListBox*>(GetDlgItem(IDC_LIST_LOGS));
	list_msg = reinterpret_cast<CListBox*>(GetDlgItem(IDC_LIST_MESSAGES));



	sbscr_msg.set(ServerModel::Event::gotChatMessage, [this]() {
		auto pointer = model.msg[model.msg.size() - 1].c_str();
		CString msg = L"Income: ";
		msg += pointer;
		list_msg->AddString(msg);
		int nCount = list_msg->GetCount();
		if (nCount > 0)list_msg->SetCurSel(nCount - 1);
	});
	model.events.subscribe(sbscr_msg);

	sbscr_bmp.set(ServerModel::Event::gotBMP, [this]() {
		putPhoto(CString(model.path_to_photo.c_str()));
	});
	model.events.subscribe(sbscr_bmp);

	sbscr_logger.set(vk::safe::Logger::Events::push_log, [this]() {
		list_logger->AddString(CString(vk::safe::Logger::getLastLog().message().c_str()));
		int nCount = list_logger->GetCount();
		if (nCount > 0)list_logger->SetCurSel(nCount - 1);
	});
	vk::safe::Logger::subscribe(sbscr_logger);



	return TRUE; 
}