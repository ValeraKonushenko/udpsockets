#pragma once
#include "../vk/SocketServer.h"
#include "../vk/ThreadManager.h"
#include "../vk/observer/Observer.h"
#include "json.h"
#include <string>


class ServerModel {
public:
	using Address = vk::UDP::safe::Address;
	using Package = vk::UDP::safe::Package;	
	using _PackData = vk::UDP::safe::_PackData;
private:
	vk::unsafe::Container<char> getFileContentB(std::wstring_view path)const noexcept(false);
	void goRecData()noexcept(false);
	void ifDataWasGot(Address& adr, 
					  Package& package, 
					  _PackData& header,
					  bool &wait_for_body,
					  int recieved_b)noexcept(false);
	void onGotMessage(const Address& adr, 
					  const Package& p,
					  const nlohmann::json &json,
					  int recieved_b)noexcept(false);
	void onGotAction(const Address& adr,
					 const Package& p,
					 const nlohmann::json& json,
					 int recieved_b)noexcept(false);
	void onGotUnknow(const Address& adr,
					 const Package& p,
					 const nlohmann::json& json,
					 int recieved_b)noexcept(false);
	void sendAnswerB(const Address& adr, int flag)noexcept(false);
public:

	std::vector<std::wstring> msg;
	enum Event {
		gotChatMessage,
		gotBMP
	};
	std::string path_to_photo;
	vk::safe::Observer events;
	std::atomic_bool is_run;
	vk::safe::ThreadManager threads;
	vk::UDP::safe::UDPSocket server;
	void openServer(std::wstring port)noexcept(false);
	void closeServer()noexcept(false);
	void startRecvData()noexcept;

	void sendBmp(u_long port, const wchar_t* host, std::wstring path)noexcept(false);
	void sendStart(u_long port, const wchar_t* host)noexcept(false);
	void sendMessage(u_long port, const wchar_t* host, std::wstring msg)noexcept(false);
	ServerModel()noexcept(false);
	~ServerModel();
	ServerModel(const ServerModel&)				= delete;
	ServerModel(ServerModel&&)					= delete;
	ServerModel& operator=(const ServerModel&)	= delete;
	ServerModel& operator=(ServerModel&&)		= delete;
};