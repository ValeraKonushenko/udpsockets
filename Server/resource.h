//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Server.rc
//
#define IDD_SERVER_DIALOG               102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON_START                1000
#define IDC_EDIT_PORT                   1001
#define IDC_EDIT_HOST                   1002
#define IDC_BUTTON_STOP                 1003
#define IDC_LIST_LOGS                   1004
#define IDC_LIST_MESSAGES               1005
#define IDC_BUTTON_CLEAR_LOGS           1006
#define IDC_BUTTON_CLEAR_MSG            1007
#define IDC_BUTTON_START_TO             1008
#define IDC_EDIT_PORT_TO                1009
#define IDC_EDIT_HOST_TO                1010
#define IDC_BUTTON_STOP_TO              1011
#define IDC_EDIT_MSG                    1012
#define IDC_BUTTON_SEND_MSG             1013
#define IDC_BUTTON_SEND_FILE            1014
#define IIDC_OUTPUT_BMP                 1016
#define IDC_OUTPUT_BMP                  1017

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1018
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
