#pragma once

#include "resource.h"	
class CServerApp : public CWinApp{
public:
	CServerApp();
	virtual BOOL InitInstance();
	DECLARE_MESSAGE_MAP()
};

extern CServerApp theApp;
